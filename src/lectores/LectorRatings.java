package lectores;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import VOS.VORating;
import au.com.bytecode.opencsv.CSVReader;
import data_structures.ListaEncadenada;

public class LectorRatings
{
	private ListaEncadenada<VORating> ratings;

	private static final String RUTA_RATINGS = "data/ratings_filtered.csv";

	public LectorRatings()
	{
		System.out.println("**Leyendo el archivo de ratings**");
		try
		{
			ratings = new ListaEncadenada<VORating>();
			CSVReader reader = new CSVReader(new FileReader(RUTA_RATINGS), ',' , '"' , 1);
			leer(reader);
			reader.close();
		}
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Se cargaron " + ratings.darNumeroElementos() + " ratings");
		System.out.println();
	}

	private void leer(CSVReader reader)
	{
		try
		{
			String[] nextline;
			while((nextline = reader.readNext()) != null)
			{
				VORating nuevo = new VORating();
				nuevo.setUserId(Integer.parseInt(nextline[0]));
				nuevo.setMovieId(Integer.parseInt(nextline[1]));
				double rat = Double.parseDouble(nextline[2]);
				nuevo.setRating(rat);
				
				ratings.agregarElementoFinal(nuevo);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public ListaEncadenada<VORating> darListaRatings()
	{
		return ratings;
	}
}

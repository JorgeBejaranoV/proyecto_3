package lectores;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import VOS.VOFranquicia;
import VOS.VOTeatro;
import VOS.VOUbicacion;
import data_structures.ListaEncadenada;

public class LectorTeatros 
{
	private ListaEncadenada<VOTeatro> teatros;
	
	public LectorTeatros()
	{
		teatros = new ListaEncadenada<>();
		leer();
	}
	
	private void leer()
	{

		JSONParser parser = new JSONParser();
		try 
		{

			Object obj = parser.parse(new FileReader("./data/teatros/teatros.json.txt"));


			JSONArray arreglo = (JSONArray) obj;

			for (int i=0;i<arreglo.size();i++)
			{

				VOTeatro nuevo = new VOTeatro();
				JSONObject ob =(JSONObject) arreglo.get(i);

				// se obtiene el nombre del teatro
				String nombre = (String)ob.get("Nombre");
			

				// ubicaci�n geogr�fica del teatro
				String ubicacionGeograficas = (String)ob.get("Ubicacion geografica (Lat|Long)");
			
				
				// franquicia del teatro ya mencionado
				String franquicia = (String)ob.get("Franquicia");
				
				nuevo.setNombre(nombre);
				VOFranquicia fran = new VOFranquicia();
				fran.setNombre(franquicia);
				nuevo.setFranquicia(fran);
				
				VOUbicacion ubicacion = new VOUbicacion();
				String[] xUbi = ubicacionGeograficas.split("\\|");
				double latitud = Double.parseDouble(xUbi[0]);
				double longitud = Double.parseDouble(xUbi[1]);
				ubicacion.setLatitud(latitud);
				ubicacion.setLongitud(longitud);
				nuevo.setUbicacion(ubicacion);
				
				teatros.agregarElementoFinal(nuevo);
			}
		}

		catch (FileNotFoundException e) {e.printStackTrace();	} catch (IOException e) {e.printStackTrace();} catch (ParseException e) {e.printStackTrace();}

	}
	
	public ListaEncadenada<VOTeatro> darTeatros()
	{
		return teatros;
	}
	
}

package lectores.noUtilizados;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import VOS.VODistancia;
import au.com.bytecode.opencsv.CSVReader;
import data_structures.ListaEncadenada;

public class LectorDistancias 
{
	private ListaEncadenada<VODistancia> distancias;

	private static final String RUTA_DISTANCIAS = "data/distancias.csv";

	public LectorDistancias()
	{
		try
		{
			System.out.println("* Leyendo archivo de distancias");
			distancias = new ListaEncadenada<VODistancia>();
			CSVReader reader = new CSVReader(new FileReader(RUTA_DISTANCIAS), ',' , '"' , 1);
			leer(reader);
			reader.close();
		}
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void leer(CSVReader reader)
	{
		try
		{
			String[] nextline;
			while((nextline = reader.readNext()) != null)
			{
				VODistancia nueva = new VODistancia();
				nueva.setTeatro1(nextline[0]);
				nueva.setTeatro2(nextline[1]);
				double distancia = Double.parseDouble(nextline[2]);
				nueva.setDistancia(distancia);
				
				distancias.agregarElementoFinal(nueva);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public ListaEncadenada<VODistancia> darListaDistancias()
	{
		return distancias;
	}
}

package lectores.noUtilizados;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import VOS.VOTiempo;
import data_structures.ListaEncadenada;

public class LectorTiemposDistancias 
{
	private ListaEncadenada<VOTiempo> listaTiempos;

	public LectorTiemposDistancias()
	{
		listaTiempos = new ListaEncadenada<>();
		leer();
	}

	private void leer()
	{
		System.out.println("* Leyendo archivo de tiempos");
		JSONParser parser = new JSONParser();
		try 
		{

			Object obj = parser.parse(new FileReader("./data/tiemposIguales.json"));


			JSONArray arreglo = (JSONArray) obj;

			for (int i=0;i<arreglo.size();i++){

				try
				{
					JSONObject ob =(JSONObject) arreglo.get(i);

					// se obtiene el nombre de los teatros
					String teatro1 = (String)ob.get("Teatro 1");


					String teatro2 = (String)ob.get("Teatro 2");
					System.out.println(teatro2);
					// se tiene la información de el tiempo que se demora en llevar de un teatro a otro
					Integer distanciaTiempo = (Integer)ob.get("Tiempo (minutos)");

					VOTiempo nuevo = new VOTiempo();
					nuevo.setOrigen(teatro1);
					nuevo.setDestino(teatro2);
					nuevo.setTiempo(distanciaTiempo);
				}
				catch(Exception e)
				{
					
				}

			}
		}

		catch (FileNotFoundException e) {e.printStackTrace();	} catch (IOException e) {e.printStackTrace();} catch (ParseException e) {e.printStackTrace();}

	}

	public ListaEncadenada<VOTiempo> darListaTiempos()
	{
		return listaTiempos;
	}

}

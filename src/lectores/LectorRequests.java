package lectores;

import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import auxiliaresCarga.DataRatingRequest;
import auxiliaresCarga.DataRequest;

public class LectorRequests
{
	private DataRequest request;
	private String rutaJson;
	
	public LectorRequests(String rutaJson)
	{
		System.out.println("**Leyendo petici�n**");
		this.rutaJson = rutaJson;
		leer();
	}
	
	private void leer()
	{
		JSONParser parser = new JSONParser();
		try 
		{
			Object obj = parser.parse(new FileReader(rutaJson));
			JSONObject req = (JSONObject) obj;
			JSONObject request =(JSONObject)req.get("request");
			
			DataRequest data = new DataRequest();
			String name = (String)request.get("user_name");
			data.setUser_name(name);
			
			
			JSONArray rat = (JSONArray) request.get("ratings");
			DataRatingRequest[] dataRequest = new DataRatingRequest[rat.size()];
			for(int i=0; i < rat.size(); i++)
			{
				JSONObject o1 = (JSONObject) rat.get(i);
				DataRatingRequest dataRating = new DataRatingRequest();
				dataRating.setMovieId((Long)o1.get("item_id"));
				dataRating.setRating((Long)o1.get("rating"));
				dataRequest[i] = dataRating;
			}
			data.setRatings(dataRequest);
			this.request = data;
		} 
		catch (IOException | ParseException e) 
		{
			e.printStackTrace();
		}
		System.out.println("Se carg� la petici�n");
		System.out.println("Nombre: " + request.getUser_name());
		System.out.println("Ratings: " + request.getRatings().length);
		System.out.println();
	}
	
	public DataRequest darRequest()
	{
		return request;
	}
}

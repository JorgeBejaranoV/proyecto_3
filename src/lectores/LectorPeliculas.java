package lectores;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import VOS.VOPelicula;
import auxiliaresCarga.DataMoviesFiltered;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import data_structures.ListaEncadenada;

public class LectorPeliculas
{
	private static final String RUTA_JSON = "data/movies_filtered.json";
	private ListaEncadenada<VOPelicula> listaPeliculas;
	
	public LectorPeliculas()
	{
		leer();
	}
	
	private void leer()
	{
		System.out.println("**Leyendo archivo de películas**");
		listaPeliculas = new ListaEncadenada<VOPelicula>();
		Gson gson = new GsonBuilder()
		.registerTypeAdapter(int.class, integerAdapterFromString)
		.registerTypeAdapter(double.class, doubleAdapterFromString)
		.registerTypeAdapter(float.class, floatAdapterFromString)
		.create();
		
		try(FileReader fr = new FileReader(RUTA_JSON))
		{
			DataMoviesFiltered[] movies = gson.fromJson(fr, DataMoviesFiltered[].class);
			for(DataMoviesFiltered data:movies)
			{
				int id = Integer.parseInt(data.getMovieId());
				String title = data.getTitle();
				String[] genres = data.getGenres().split("\\|");
				VOPelicula nueva = new VOPelicula(id, title, genres);
				
				listaPeliculas.agregarElementoFinal(nueva);
			}
		} 
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Se cargaron " + listaPeliculas.darNumeroElementos() + " peliculas");
		System.out.println();
	}
	
	private static final TypeAdapter<Integer> integerAdapterFromString = new TypeAdapter<Integer>() {
		@Override 
		public void write(JsonWriter out, Integer value) throws IOException {
			if (value == null) {
				out.nullValue();
			} else {
				out.value(value);
			}
		}
		@Override 
		public Integer read(JsonReader in) throws IOException {
			JsonToken peek = in.peek();
			switch (peek) {
			case NULL:
				return -1;
			case NUMBER:
				return in.nextInt();
			case STRING:
				String s = in.nextString();	
				s = s.substring(0, 4);  
				return Integer.parseInt(s);
			default:
				throw new IllegalStateException("Expected NUMBER but was " + peek);
			}
		}
	};


	private static final TypeAdapter<Double> doubleAdapterFromString = new TypeAdapter<Double>() {
		@Override 
		public void write(JsonWriter out, Double value) throws IOException {
			if (value == null) {
				out.nullValue();
			} else {
				out.value(value);
			}
		}
		@Override 
		public Double read(JsonReader in) throws IOException {
			JsonToken peek = in.peek();
			switch (peek) {
			case NULL:
				return -1.0;
			case NUMBER:
				return in.nextDouble();
			case STRING:
				String s = in.nextString();	 
				if(s.equals("N/A"))
				{
					return 0.0;
				}
				return Double.parseDouble(s);
			default:
				throw new IllegalStateException("Expected NUMBER but was " + peek);
			}
		}
	};	
	private static final TypeAdapter<Float> floatAdapterFromString = new TypeAdapter<Float>() {
		@Override 
		public void write(JsonWriter out, Float value) throws IOException {
			if (value == null) {
				out.nullValue();
			} else {
				out.value(value);
			}
		}
		@Override 
		public Float read(JsonReader in) throws IOException {
			JsonToken peek = in.peek();
			switch (peek) {
			case NULL:
				return -1.0f;
			case NUMBER:
				return (float) in.nextDouble();
			case STRING:
				String s = in.nextString();	 
				if(s.equals("N/A"))
				{
					return (float) 0.0;
				}
				return Float.parseFloat(s);
			default:
				throw new IllegalStateException("Expected NUMBER but was " + peek);
			}
		}
	};	
	
	public ListaEncadenada<VOPelicula> darListaPeliculas()
	{
		return listaPeliculas;
	}
}

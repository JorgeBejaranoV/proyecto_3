package lectores;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import auxiliaresCarga.DataFuncion;
import auxiliaresCarga.DataFuncionesPelicula;
import data_structures.ListaEncadenada;

public class LectorFuncionesDias
{

	private ListaEncadenada<DataFuncion> listaDia1;
	private ListaEncadenada<DataFuncion> listaDia2;
	private ListaEncadenada<DataFuncion> listaDia3;
	private ListaEncadenada<DataFuncion> listaDia4;
	private ListaEncadenada<DataFuncion> listaDia5;

	public LectorFuncionesDias()
	{
		//Se inicializan las listas de los d�as
		listaDia1 = new ListaEncadenada<>();
		listaDia2 = new ListaEncadenada<>();
		listaDia3 = new ListaEncadenada<>();
		listaDia4 = new ListaEncadenada<>();
		listaDia5 = new ListaEncadenada<>();

		//Leo cada uno de los archivos para guardar los datos en las listas
		leerDia1();
		leerDia2();
		leerDia3();
		leerDia4();
		leerDia5();
	}

	public void leerDia1()
	{

		JSONParser parser = new JSONParser();
		try 
		{

			Object obj = parser.parse(new FileReader("./data/programacion/dia1.json"));

			JSONArray arreglo = (JSONArray) obj;
			DataFuncion[] arregloTeatros = new DataFuncion[arreglo.size()];

			for (int i=0;i<arreglo.size();i++)
			{

				JSONObject ob =(JSONObject) arreglo.get(i);

				// Extraigo el nombre del teatro del JSON
				String  Teatros =(String) ob.get("teatros");

				//Lo asigno al objeto DataFuncion
				arregloTeatros[i] = new DataFuncion();
				arregloTeatros[i].setNombreTeatro(Teatros);



				JSONObject teatro = (JSONObject) ob.get("teatro");

				JSONArray arregloPeliculas = (JSONArray)teatro.get("peliculas");
				DataFuncionesPelicula[] arregloPelis = new DataFuncionesPelicula[arregloPeliculas.size()];

				for( int j = 0; j< arregloPeliculas.size(); j++)
				{
					JSONObject ob2 =(JSONObject) arregloPeliculas.get(j);

					//este id provee el id de la pel�cula
					Long id = (long) ob2.get("id");
					arregloPelis[j] = new DataFuncionesPelicula();
					arregloPelis[j].setMovieId(id);

					JSONArray funciones = (JSONArray) ob2.get("funciones");
					String[] horas = new String[funciones.size()];

					for(int k = 0; k< funciones.size(); k++)
					{
						JSONObject ob3 = (JSONObject) funciones.get(k);
						// el horario da la hora con el am o pm del id de la pel�cula de arriba
						String horario = (String) ob3.get("hora");
						horas[k] = horario;
					}
					arregloPelis[j].setHoras(horas);
				}

				arregloTeatros[i].setFunciones(arregloPelis);

				//Se guarda el objeto en la lista
				listaDia1.agregarAlInicio(arregloTeatros[i]);
			}


		}

		catch (FileNotFoundException e) 
		{
			e.printStackTrace();	
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
		}

	}


	public void leerDia2()
	{

		JSONParser parser = new JSONParser();
		try 
		{

			Object obj = parser.parse(new FileReader("./data/programacion/dia2.json"));

			JSONArray arreglo = (JSONArray) obj;
			DataFuncion[] arregloTeatros = new DataFuncion[arreglo.size()];

			for (int i=0;i<arreglo.size();i++)
			{

				JSONObject ob =(JSONObject) arreglo.get(i);

				// Extraigo el nombre del teatro del JSON
				String  Teatros =(String) ob.get("teatros");

				//Lo asigno al objeto DataFuncion
				arregloTeatros[i] = new DataFuncion();
				arregloTeatros[i].setNombreTeatro(Teatros);


				JSONObject teatro = (JSONObject) ob.get("teatro");

				JSONArray arregloPeliculas = (JSONArray)teatro.get("peliculas");
				DataFuncionesPelicula[] arregloPelis = new DataFuncionesPelicula[arregloPeliculas.size()];

				for( int j = 0; j< arregloPeliculas.size(); j++)
				{
					JSONObject ob2 =(JSONObject) arregloPeliculas.get(j);

					//este id provee el id de la pel�cula
					Long id = (long) ob2.get("id");
					arregloPelis[j] = new DataFuncionesPelicula();
					arregloPelis[j].setMovieId(id);

					JSONArray funciones = (JSONArray) ob2.get("funciones");
					String[] horas = new String[funciones.size()];

					for(int k = 0; k< funciones.size(); k++)
					{
						JSONObject ob3 = (JSONObject) funciones.get(k);
						// el horario da la hora con el am o pm del id de la pel�cula de arriba
						String horario = (String) ob3.get("hora");
						horas[k] = horario;
					}
					arregloPelis[j].setHoras(horas);
				}

				arregloTeatros[i].setFunciones(arregloPelis);

				//Se guarda el objeto en la lista
				listaDia2.agregarAlInicio(arregloTeatros[i]);
			}
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();	
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
	}

	public void leerDia3()
	{

		JSONParser parser = new JSONParser();
		try 
		{

			Object obj = parser.parse(new FileReader("./data/programacion/dia3.json"));

			JSONArray arreglo = (JSONArray) obj;
			DataFuncion[] arregloTeatros = new DataFuncion[arreglo.size()];

			for (int i=0;i<arreglo.size();i++)
			{

				JSONObject ob =(JSONObject) arreglo.get(i);

				// Extraigo el nombre del teatro del JSON
				String  Teatros =(String) ob.get("teatros");

				//Lo asigno al objeto DataFuncion
				arregloTeatros[i] = new DataFuncion();
				arregloTeatros[i].setNombreTeatro(Teatros);


				JSONObject teatro = (JSONObject) ob.get("teatro");

				JSONArray arregloPeliculas = (JSONArray)teatro.get("peliculas");
				DataFuncionesPelicula[] arregloPelis = new DataFuncionesPelicula[arregloPeliculas.size()];

				for( int j = 0; j< arregloPeliculas.size(); j++)
				{
					JSONObject ob2 =(JSONObject) arregloPeliculas.get(j);

					//este id provee el id de la pel�cula
					Long id = (long) ob2.get("id");
					arregloPelis[j] = new DataFuncionesPelicula();
					arregloPelis[j].setMovieId(id);

					JSONArray funciones = (JSONArray) ob2.get("funciones");
					String[] horas = new String[funciones.size()];

					for(int k = 0; k< funciones.size(); k++)
					{
						JSONObject ob3 = (JSONObject) funciones.get(k);
						// el horario da la hora con el am o pm del id de la pel�cula de arriba
						String horario = (String) ob3.get("hora");
						horas[k] = horario;
					}
					arregloPelis[j].setHoras(horas);
				}

				arregloTeatros[i].setFunciones(arregloPelis);

				//Se guarda el objeto en la lista
				listaDia3.agregarAlInicio(arregloTeatros[i]);
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();	
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
		}



	}
	public void leerDia4()
	{

		JSONParser parser = new JSONParser();
		try 
		{

			Object obj = parser.parse(new FileReader("./data/programacion/dia4.json"));

			JSONArray arreglo = (JSONArray) obj;
			DataFuncion[] arregloTeatros = new DataFuncion[arreglo.size()];

			for (int i=0;i<arreglo.size();i++)
			{

				JSONObject ob =(JSONObject) arreglo.get(i);

				// Extraigo el nombre del teatro del JSON
				String  Teatros =(String) ob.get("teatros");

				//Lo asigno al objeto DataFuncion
				arregloTeatros[i] = new DataFuncion();
				arregloTeatros[i].setNombreTeatro(Teatros);


				JSONObject teatro = (JSONObject) ob.get("teatro");

				JSONArray arregloPeliculas = (JSONArray)teatro.get("peliculas");
				DataFuncionesPelicula[] arregloPelis = new DataFuncionesPelicula[arregloPeliculas.size()];

				for( int j = 0; j< arregloPeliculas.size(); j++)
				{
					JSONObject ob2 =(JSONObject) arregloPeliculas.get(j);

					//este id provee el id de la pel�cula
					Long id = (long) ob2.get("id");
					arregloPelis[j] = new DataFuncionesPelicula();
					arregloPelis[j].setMovieId(id);

					JSONArray funciones = (JSONArray) ob2.get("funciones");
					String[] horas = new String[funciones.size()];

					for(int k = 0; k< funciones.size(); k++)
					{
						JSONObject ob3 = (JSONObject) funciones.get(k);
						// el horario da la hora con el am o pm del id de la pel�cula de arriba
						String horario = (String) ob3.get("hora");
						horas[k] = horario;
					}
					arregloPelis[j].setHoras(horas);
				}

				arregloTeatros[i].setFunciones(arregloPelis);

				//Se guarda el objeto en la lista
				listaDia4.agregarAlInicio(arregloTeatros[i]);
			}
		}

		catch (FileNotFoundException e) 
		{
			e.printStackTrace();	
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void leerDia5()
	{

		JSONParser parser = new JSONParser();
		try 
		{

			Object obj = parser.parse(new FileReader("./data/programacion/dia5.json"));

			JSONArray arreglo = (JSONArray) obj;
			DataFuncion[] arregloTeatros = new DataFuncion[arreglo.size()];

			for (int i=0;i<arreglo.size();i++)
			{

				JSONObject ob =(JSONObject) arreglo.get(i);

				// Extraigo el nombre del teatro del JSON
				String  Teatros =(String) ob.get("teatros");

				//Lo asigno al objeto DataFuncion
				arregloTeatros[i] = new DataFuncion();
				arregloTeatros[i].setNombreTeatro(Teatros);


				JSONObject teatro = (JSONObject) ob.get("teatro");

				JSONArray arregloPeliculas = (JSONArray)teatro.get("peliculas");
				DataFuncionesPelicula[] arregloPelis = new DataFuncionesPelicula[arregloPeliculas.size()];

				for( int j = 0; j< arregloPeliculas.size(); j++)
				{
					JSONObject ob2 =(JSONObject) arregloPeliculas.get(j);

					//este id provee el id de la pel�cula
					Long id = (long) ob2.get("id");
					arregloPelis[j] = new DataFuncionesPelicula();
					arregloPelis[j].setMovieId(id);

					JSONArray funciones = (JSONArray) ob2.get("funciones");
					String[] horas = new String[funciones.size()];

					for(int k = 0; k< funciones.size(); k++)
					{
						JSONObject ob3 = (JSONObject) funciones.get(k);
						// el horario da la hora con el am o pm del id de la pel�cula de arriba
						String horario = (String) ob3.get("hora");
						horas[k] = horario;
					}
					arregloPelis[j].setHoras(horas);
				}

				arregloTeatros[i].setFunciones(arregloPelis);

				//Se guarda el objeto en la lista
				listaDia5.agregarAlInicio(arregloTeatros[i]);
			}
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();	
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
		catch (ParseException e)
		{
			e.printStackTrace();
		}


	}
	

	public ListaEncadenada<DataFuncion> listaDia1()
	{
		return listaDia1;
	}
	

	public ListaEncadenada<DataFuncion> listaDia2()
	{
		return listaDia2;
	}
	

	public ListaEncadenada<DataFuncion> listaDia3()
	{
		return listaDia3;
	}
	

	public ListaEncadenada<DataFuncion> listaDia4()
	{
		return listaDia4;
	}
	

	public ListaEncadenada<DataFuncion> listaDia5()
	{
		return listaDia5;
	}

}

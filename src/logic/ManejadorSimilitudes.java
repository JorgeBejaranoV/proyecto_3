package logic;

import java.util.Iterator;

import auxiliaresCarga.DataMatriz;
import data_structures.EncadenamientoSeparadoTH;
import data_structures.ListaEncadenada;
import lectores.LectorSimilitudes;

/**
 * Clase responsable del c�lculo de las similitudes
 */
public class ManejadorSimilitudes
{
	//-----------------------------------------------------------------------------
	// ATRIBUTOS
	//-----------------------------------------------------------------------------
	/**
	 * Encargado de la lectura del archivo
	 */
	private LectorSimilitudes lectorSimilitudes;

	/**
	 * Tabla con las similitudes por pel�cula
	 */
	private EncadenamientoSeparadoTH<Integer, DataMatriz> tablaSimilitudes;

	/**
	 * Atributo que modela si ya se carg� la tabla de similitudes o no
	 */
	private boolean estadoCarga;

	//-----------------------------------------------------------------------------
	// M�TODOS
	//-----------------------------------------------------------------------------

	/**
	 * Constructor
	 */
	public ManejadorSimilitudes()
	{
		lectorSimilitudes = new LectorSimilitudes();
		estadoCarga = crearTabla();
		if(estadoCarga == false)
		{
			System.out.println("No se carg� la tabla de similitudes");
		}
		else
		{
			System.out.println("Se carg� la tabla de similitudes");
		}
		System.out.println();
	}

	/**
	 * M�todo que calcula la similitud entre dos pel�culas
	 * @param idMovie1 pel�cula de la cual se parte
	 * @param idMovie2 similitud buscada
	 * @return valor de la similitud, 0 si no existe similitud alguna
	 */
	public double darSimilitud(int idMovie1, int idMovie2)
	{
		double respuesta = 0;
		
		DataMatriz similitud = tablaSimilitudes.darValor(idMovie1);
		if(similitud != null)
		{
			AuxiliarBusquedaSimilitud aux = new AuxiliarBusquedaSimilitud(similitud, idMovie2);
			respuesta = aux.darSimilitud();
		}
		else
		{
			System.out.println("ERROR: No se hall� la similitud");
		}
		return respuesta;
	}

	/**
	 * Crea la tabla de similitudes para facilitar la b�squeda
	 */
	private boolean crearTabla()
	{
		ListaEncadenada<DataMatriz> lista = lectorSimilitudes.darListaSimilitudes();
		tablaSimilitudes = new EncadenamientoSeparadoTH<>(lista.darNumeroElementos()*2);
		Iterator<DataMatriz> iter = lista.iterator();
		while(iter.hasNext())
		{
			try
			{
				DataMatriz actual = iter.next();
				tablaSimilitudes.insertar(actual.getMovieId(), actual);
			}
			catch(Exception e)
			{

			}
		}
		if(tablaSimilitudes.estaVacia())
		{
			return false;
		}
		return true;	
	}
}


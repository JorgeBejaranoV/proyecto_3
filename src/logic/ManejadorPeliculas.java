package logic;

import java.util.Iterator;

import VOS.VOPelicula;
import data_structures.EncadenamientoSeparadoTH;
import data_structures.ListaEncadenada;

/**
 * Clase encargada de manejar la cartelera general de pel�culas
*/

public class ManejadorPeliculas 
{
	//-----------------------------------------------------------------------------
	// ATRIBUTOS
	//-----------------------------------------------------------------------------
	
	/**
	 * Tabla de hash con pel�culas
	 */
	private EncadenamientoSeparadoTH<Long, VOPelicula> tablaPeliculas;
	
	/**
	 * Lista con pel�culas
	 */
	private ListaEncadenada<VOPelicula> listaPeliculas;
	
	//-----------------------------------------------------------------------------
	// M�TODOS
	//-----------------------------------------------------------------------------
	
	/**
	 * Constructor de la clase manejadora de pel�culas
	 * @param listaPeliculas
	 */
	public ManejadorPeliculas(ListaEncadenada<VOPelicula> listaPeliculas)
	{
		this.listaPeliculas = listaPeliculas;
		construirTabla();
	}
	
	/**
	 * Construye la tabla de pel�culas
	 */
	private void construirTabla()
	{
		tablaPeliculas = new EncadenamientoSeparadoTH<>(2*listaPeliculas.darNumeroElementos());
		Iterator<VOPelicula> iter = listaPeliculas.iterator();
		while(iter.hasNext())
		{
			VOPelicula actual = iter.next();
			tablaPeliculas.insertar(actual.getId(), actual);
		}
	}
	
	/**
	 * M�todo para buscar una pel�cula por su id
	 * @param id de la pel�cula
	 * @return pel�cula buscada
	 */
	public VOPelicula getPelicula(long id)
	{
		VOPelicula resp = tablaPeliculas.darValor(id);
		return resp;
	}
	
	/**
	 * Retorna la cantidad de pel�culas en el sistema
	 * @return n�mero de pel�culas
	 */
	public int sizeMovies()
	{
		return listaPeliculas.darNumeroElementos();
	}
	
}

package logic;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import VOS.VOPelicula;
import VOS.VOPeliculaPlan;
import VOS.VOTeatro;
import auxiliaresCarga.DataFuncion;
import auxiliaresCarga.DataFuncionesPelicula;
import data_structures.EncadenamientoSeparadoTH;
import data_structures.HashTable;
import data_structures.ListaEncadenada;
import data_structures.RedBlackBST;

/**
 * Clase encargada de enlazar las funciones a los teatros
 */
public class ManejadorCartelera
{
	private final static String TEATRO_POR_DEFECTO = "Cine Colombia Cedritos";
	
	//-----------------------------------------------------------------------------
	// ATRIBUTOS
	//-----------------------------------------------------------------------------

	/**
	 * Tabla de hash con las pel�culas en cartelera
	 */
	private ManejadorPeliculas manejadorPeliculas;

	/**
	 * Lista de los teatros
	 */
	private ListaEncadenada<VOTeatro> listaTeatros;

	/**
	 * Arreglo con el contenido de las funciones por d�a
	 */
	private ListaEncadenada<DataFuncion>[] arregloFunciones;
	
	/**
	 * Lista con las funciones
	 */
	private ListaEncadenada<VOPeliculaPlan> listaFunciones;

	/**
	 * Tabla de hash en donde en cada posici�n se tiene un teatro y la lista de peliculas plan por teatro
	 */
	private EncadenamientoSeparadoTH<String, RedBlackBST<Long, ListaEncadenada<VOPeliculaPlan>>>[] funcionesDia;
	private HashTable<String,ListaEncadenada<VOPeliculaPlan>> tablaFunciones;
	
	//-----------------------------------------------------------------------------
	// M�TODOS
	//-----------------------------------------------------------------------------

	/**
	 * Constructor del manejador de carga
	 * @param tablaPeliculas
	 * @param listaTeatros
	 * @param arregloFunciones
	 */
	public ManejadorCartelera(ManejadorPeliculas manejadorPeliculas, ListaEncadenada<VOTeatro> listaTeatros, ListaEncadenada<DataFuncion>[] arregloFunciones)
	{
		this.manejadorPeliculas = manejadorPeliculas;
		this.listaTeatros = listaTeatros;
		this.arregloFunciones = arregloFunciones;
		crearTablaFunciones();
		crearTablaFuncionesOld();
	}

	/**
	 * M�todo encargado de crear los objetos de tipo VOPeliculaPlan
	 */
	/**
	 * M�todo encargado de crear los objetos de tipo VOPeliculaPlan
	 */
	private void crearTablaFuncionesOld()
	{
		System.out.println("**Creando la tabla de funciones**");

		tablaFunciones = new HashTable<>(100);

		//Recorrido por cada posici�n(D�A) del arreglo
		for(int i=0; i < arregloFunciones.length; i++)
		{
			//Chequea si existe la lista en la posici�n
			if(arregloFunciones[i] != null)
			{
				//Guarda el d�a
				int dia = i;

				ListaEncadenada<DataFuncion> listaActual = arregloFunciones[i];
				Iterator<DataFuncion> iter = listaActual.iterator();
				
				while(iter.hasNext())
				{
					DataFuncion actual = iter.next();

					//Guarda el nombre del teatro
					String nombreTeatro = actual.getNombreTeatro();
					VOTeatro teatro = buscarTeatro(nombreTeatro);

					//Guarda el arreglo de funciones
					DataFuncionesPelicula[] funciones = actual.getFunciones();
					ListaEncadenada <VOPeliculaPlan> lista= new ListaEncadenada<>();
					//Recorre el arreglo de funciones del teatro
					for(int j=0; j < funciones.length; j++)
					{

						DataFuncionesPelicula funcion = funciones[j];

						//Guarda el id de la pel�cula
						long movieId = funcion.getMovieId();
						VOPelicula peli = buscarPelicula(movieId);

						//Recorro las horas de las funciones
						for(String hora:funcion.getHoras())
						{
							try
							{
								
								
								SimpleDateFormat format = new SimpleDateFormat("HH:mm");
								Date d1 = format.parse(hora);

								Calendar horaInicial = Calendar.getInstance();
								horaInicial.setTime(d1);

								Calendar horaFinal = horaInicial;
								horaFinal.add(Calendar.HOUR_OF_DAY, 1);

								VOPeliculaPlan plan = new VOPeliculaPlan();
								plan.setDia(dia);
								plan.setPelicula(peli);
								plan.setTeatro(teatro);
								plan.setHoraInicio(horaInicial);
								plan.setHoraFin(horaFinal);
								
								lista.agregarElementoFinal(plan);

								
							}
							catch(ParseException e)
							{
								e.printStackTrace();
							} catch (java.text.ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

					}
				
						tablaFunciones.insertar(nombreTeatro, lista);
					
					
				}

			}
		}
		if(tablaFunciones.estaVacia())
		{
			System.out.println("No se cre� la tabla");
			System.out.println();
		}
		else
		{
			System.out.println("Tabla creada");
			System.out.println();
		}
	}

	private void crearTablaFunciones()
	{
		System.out.println("**Creando la tabla de funciones**");
		listaFunciones = new ListaEncadenada<>();
		funcionesDia = (EncadenamientoSeparadoTH<String, RedBlackBST<Long, ListaEncadenada<VOPeliculaPlan>>>[]) new EncadenamientoSeparadoTH[5];

		//Recorrido por cada posici�n(D�A) del arreglo
		for(int i=0; i < arregloFunciones.length; i++)
		{
			funcionesDia[i] = new EncadenamientoSeparadoTH<>(100);
			//Chequea si existe la lista en la posici�n
			if(arregloFunciones[i] != null)
			{
				//Guarda el d�a
				int dia = i;

				ListaEncadenada<DataFuncion> listaActual = arregloFunciones[i];
				Iterator<DataFuncion> iter = listaActual.iterator();
				while(iter.hasNext())
				{
					DataFuncion actual = iter.next();

					//Guarda el nombre del teatro
					String nombreTeatro = actual.getNombreTeatro();
					VOTeatro teatro = buscarTeatro(nombreTeatro);

					//Guarda el arreglo de funciones
					DataFuncionesPelicula[] funciones = actual.getFunciones();

					//Recorre el arreglo de funciones del teatro
					for(int j=0; j < funciones.length; j++)
					{

						DataFuncionesPelicula funcion = funciones[j];

						//Guarda el id de la pel�cula
						long movieId = funcion.getMovieId();
						VOPelicula peli = manejadorPeliculas.getPelicula(movieId);

						//Recorro las horas de las funciones
						for(String hora:funcion.getHoras())
						{
							try
							{
								SimpleDateFormat format = new SimpleDateFormat("HH:mm");
								Date d1 = format.parse(hora);
								
								Calendar horaInicial=Calendar.getInstance();
								horaInicial.set(Calendar.HOUR, d1.getHours());
								Calendar horaFinal=horaInicial;
								horaFinal.add(Calendar.HOUR, 2);
								VOPeliculaPlan plan = new VOPeliculaPlan();
								plan.setDia(dia);
								plan.setPelicula(peli);
								plan.setTeatro(teatro);
								plan.setHoraInicio(horaInicial);
								plan.setHoraFin(horaFinal);
								listaFunciones.agregarAlInicio(plan);
								
								if(plan.getTeatro() != null)
								{
									if(funcionesDia[i].darValor(nombreTeatro) == null)
									{
										funcionesDia[i].insertar(nombreTeatro, new RedBlackBST<>());
									}
									if(funcionesDia[i].darValor(nombreTeatro).get(peli.getId()) == null)
									{
										funcionesDia[i].darValor(nombreTeatro).put(peli.getId(), new ListaEncadenada<>());
									}
									funcionesDia[i].darValor(nombreTeatro).get(peli.getId()).agregarAlInicio(plan);
								}
								else
								{
									if(funcionesDia[i].darValor(TEATRO_POR_DEFECTO) == null)
									{
										funcionesDia[i].insertar(TEATRO_POR_DEFECTO, new RedBlackBST<>());
									}
									if((funcionesDia[i].darValor(TEATRO_POR_DEFECTO).get(peli.getId())) == null)
									{
										funcionesDia[i].darValor(TEATRO_POR_DEFECTO).put(peli.getId(), new ListaEncadenada<>());
									}
									funcionesDia[i].darValor(TEATRO_POR_DEFECTO).get(peli.getId()).agregarAlInicio(plan);
								}
							}
							catch(ParseException e)
							{
								e.printStackTrace();
							} catch (java.text.ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

					}
				}

			}
		}
		if(funcionesDia[0].estaVacia() && funcionesDia[1].estaVacia() && funcionesDia[2].estaVacia() && funcionesDia[3].estaVacia() && funcionesDia[4].estaVacia())
		{
			System.out.println("No se cre� la tabla");
			System.out.println();
		}
		else
		{
			System.out.println("Tabla creada");
			System.out.println();
		}
	}

	/**
	 * M�todo para buscar un teatro por nombre
	 * @param nombre del teatro
	 * @return teatro buscado, null si no se encuentra
	 */
	private VOTeatro buscarTeatro(String nombre)
	{
		Iterator<VOTeatro> iter = listaTeatros.iterator();
		while(iter.hasNext())
		{
			VOTeatro actual = iter.next();
			if(actual.getNombre().equals(nombre))
			{
				return actual;
			}
		}
		return null;
	}
	
	/**
	 * M�todo que todos los planes de un g�nero espec�fico
	 * @param dia d�a de programaci�n
	 * @param genero genero buscado
	 * @return lista con plan
	 */
	public ListaEncadenada<VOPeliculaPlan> darPlanesPeliculaGenero(int dia, String genero)
	{
		ListaEncadenada<VOPeliculaPlan> plan = new ListaEncadenada<>();
		
		Iterator<VOPeliculaPlan> iter = listaFunciones.iterator();
		while(iter.hasNext())
		{
			boolean esDeGenero = false;
			VOPeliculaPlan actual = iter.next();
			if(actual.getDia() == dia)
			{
				String[] generos = actual.getPelicula().getGeneros();
				
				for(int i=0; i < generos.length && !esDeGenero; i++)
				{
					if(generos[i].equals(genero))
					{
						esDeGenero = true;
					}
				}
			}
			if(esDeGenero)
			{
				plan.agregarAlInicio(actual);
			}
		}
		return plan;
	}
	
	/**
	 * M�todo que busca una programaci�n espec�fica
	 * @param nombreTeatro nombre del teatro
	 * @param movieId id de la pel�cula
	 * @return lista con funciones
	 */
	public ListaEncadenada<VOPeliculaPlan> buscarPeliculaPlan(int dia, String nombreTeatro, long movieId)
	{
		RedBlackBST<Long, ListaEncadenada<VOPeliculaPlan>> arbol = funcionesDia[dia].darValor(nombreTeatro);
		ListaEncadenada<VOPeliculaPlan> lista = arbol.get(movieId);
		return lista;
	}
	private VOPelicula buscarPelicula(long id)
	{
		return manejadorPeliculas.getPelicula(id);
	}

	public HashTable<String, ListaEncadenada<VOPeliculaPlan>> darTablaFunciones() {
		// TODO Auto-generated method stub
		return tablaFunciones;
	}
	public EncadenamientoSeparadoTH<String, RedBlackBST<Long, ListaEncadenada<VOPeliculaPlan>>>[] darTablaFunciones2() {
		// TODO Auto-generated method stub
		return funcionesDia;
	}
}

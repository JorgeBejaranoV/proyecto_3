package logic;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import VOS.VOPeliculaPlan;
import VOS.VOTeatro;
import comparadores.ComparadorHorasFunciones;
import data_structures.EncadenamientoSeparadoTH;
import data_structures.ListaEncadenada;
import data_structures.Merge;
import data_structures.Queue;

/**
 * Clase que calcula la agenda
 */
public class CalculadorAgenda 
{
	//-----------------------------------------------------------------------------
	// ATRIBUTOS
	//-----------------------------------------------------------------------------

	/**
	 * Manejador de pel�culas
	 */
	private ManejadorPeliculas manejadorPeliculas;

	/**
	 * Manejador de cartelera
	 */
	private ManejadorCartelera manejadorCartelera;

	/**
	 * Manejador de similitudes
	 */
	private ManejadorSimilitudes manejadorSimilitudes;

	/**
	 * Manejador de grafos
	 */
	private ManejadorGrafos manejadorGrafos;

	/**
	 * Manejador de usuarios
	 */
	private ManejadorUsuarios manejadorUsuarios;

	/**
	 * Constante que modela la franja de la ma�ana
	 */
	private static final String FRANJA_MA�ANA = "Ma�ana";

	/**
	 * Constante que modela la franja de la tarde
	 */
	private static final String FRANJA_TARDE = "Tarde";

	/**
	 * Constante que modela la franja de la noche
	 */
	private static final String FRANJA_NOCHE = "Noche";

	/**
	 * Constante que modela un d�a de referencia
	 */
	private static final long DAY_OF_REFERENCE = (long)1483268400000L;

	/**
	 * Constante que modela el fin del d�a
	 */
	private static final long END_OF_DAY = 1483315200L;

	/**
	 * Constante que modela el f�n de la ma�ana
	 */
	private static final long END_OF_MORNING = 1483272000L;

	/**
	 * Constante que modela el f�n de la tarde
	 */
	private static final long END_OF_AFTERNOON = 1483293600L;


	//-----------------------------------------------------------------------------
	// M�TODOS
	//-----------------------------------------------------------------------------

	/**
	 * Constructor del calculador de agenda
	 * @param manejadorCartelera cartelera del sistema
	 * @param manejadorPeliculas pel�culas del sistema
	 * @param manejadorSimilitudes similitudes del sistema
	 * @param manejadorGrafos grafos del sistema
	 * @param manejadorUsuarios usuarios del sistema
	 */
	public CalculadorAgenda(ManejadorCartelera manejadorCartelera, ManejadorPeliculas manejadorPeliculas,
			ManejadorSimilitudes manejadorSimilitudes,ManejadorGrafos manejadorGrafos,
			ManejadorUsuarios manejadorUsuarios)
	{
		this.manejadorCartelera = manejadorCartelera;
		this.manejadorPeliculas = manejadorPeliculas;
		this.manejadorGrafos = manejadorGrafos;
		this.manejadorSimilitudes = manejadorSimilitudes;
		this.manejadorUsuarios = manejadorUsuarios;
	}

	/**
	 * REQUERIMIENTO 7
	 * Genera un plan para un usuario por un g�nero y una fecha
	 * @param genero genero de las pel�culas
	 * @param fecha fecha de visita
	 * @return Lista con plan a seguir
	 */
	public Queue<VOPeliculaPlan> planPorGeneroYDesplazamiento(String g, int fecha)
	{
		System.out.println();
		System.out.println("************");
		System.out.println("* R E Q U E R I M I E N T O 7***");
		System.out.println("************");
		System.out.println();
		System.out.println("*Calculando agenda*");
		System.out.println("*G�nero: " + g);
		System.out.println("*D�a: " + fecha);
		System.out.println();

		//Crea la lista de respuesta
		Queue<VOPeliculaPlan> colaRespuesta = new Queue<>();

		ListaEncadenada<VOPeliculaPlan> listaFunciones = manejadorCartelera.darPlanesPeliculaGenero(fecha, g);
		try
		{
			listaFunciones = (ListaEncadenada<VOPeliculaPlan>) Merge.sort(listaFunciones, new ComparadorHorasFunciones());
		}
		catch(Exception e)
		{

		}

		EncadenamientoSeparadoTH<Integer, ListaEncadenada<VOPeliculaPlan>> tablaFunciones = crearTablaFuncionesHora(listaFunciones);

		VOPeliculaPlan actualPlan = null;
		int horaFuncion = 0;
		boolean inicial = true;
		while(horaFuncion <= 22)
		{

			if(inicial == true)
			{
				ListaEncadenada<VOPeliculaPlan> auxListaFun = tablaFunciones.darValor(horaFuncion);
				if(auxListaFun != null)
				{
					actualPlan = auxListaFun.darElemento(0);
					colaRespuesta.enqueue(actualPlan);
					horaFuncion+=2;
					
				}
				inicial = false;
			}
			else
			{

				ListaEncadenada<VOPeliculaPlan> auxListaFun = tablaFunciones.darValor(horaFuncion);
				//System.out.println("Hora: " + horaFuncion + " existen: " + (auxListaFun == null));
				if(auxListaFun != null)
				{
					VOPeliculaPlan siguienteMenor = auxListaFun.darElemento(0);
					colaRespuesta.enqueue(siguienteMenor);
					horaFuncion+=3;
					
					Iterator<VOPeliculaPlan> iter = auxListaFun.iterator();
					VOPeliculaPlan alterno = null;
					double costomenor = 100;
					while(iter.hasNext())
					{

						alterno = iter.next();
						try
						{
							double costo = manejadorGrafos.darCosto(darFranja(horaFuncion), actualPlan.getTeatro().getNombre(), siguienteMenor.getTeatro().getNombre());
							if(costo < costomenor)
							{
								alterno = siguienteMenor;
								listaFunciones.agregarElementoFinal(alterno);
							}
						}
						catch(Exception e)
						{
							
						}
					}
					
					
				}
				else
				{
					horaFuncion++;
				}
			}
		}
		


		return colaRespuesta;
	}

	/**
	 * Calcula el m�ximo de pel�culas que se pueden ver en un d�a partiendo de una funci�n incial
	 * @param c1 hora de partida
	 * @return n�mero de pel�culas
	 */
	private int calcularMaximoPeliculas(Calendar c1)
	{	
		int contador = 1;
		while(c1.before(darFinDia()))
		{
			c1.add(Calendar.HOUR_OF_DAY, 2);
			contador++;
		}

		return contador;
	}

	/**
	 * Dice en qu� franja del d�a se encuentra
	 * @param t hora actual
	 * @return Franja
	 */
	private String darFranja(int hour)
	{
		if(hour >= 12 && hour <= 18)
		{
			return FRANJA_TARDE;
		}
		else if(hour >= 18 && hour <= 00)
		{
			return FRANJA_NOCHE;
		}
		else
		{
			return FRANJA_MA�ANA;
		}
	}

	/**
	 * M�todo que calcula el fin de la ma�ana
	 * @return fin de la ma�ana
	 */
	private Time darFinMa�ana()
	{
		Calendar c = Calendar.getInstance();
		Date date = new Date();
		date.setTime(END_OF_MORNING);
		return new Time(date.getHours(), date.getMinutes(), date.getSeconds());
	}

	/**
	 * M�todo que calcula el fin de la tarde
	 * @return fin de la tarde
	 */
	private Time darFinTarde()
	{
		Calendar c = Calendar.getInstance();
		Date date = new Date();
		date.setTime(END_OF_AFTERNOON);
		return new Time(date.getHours(), date.getMinutes(), date.getSeconds());
	}

	/**
	 * M�todo que calcula el fin de la noche
	 * @return fin de la noche
	 */
	private Time darFinDia()
	{
		Date date = new Date();
		date.setTime(END_OF_DAY);
		return new Time(date.getHours(), date.getMinutes(), date.getSeconds());
	}

	private VOPeliculaPlan calcularSiguienteFuncionGeneroEnTeatro(ListaEncadenada<VOPeliculaPlan> funciones, VOTeatro teatro)
	{
		Iterator<VOPeliculaPlan> iter = funciones.iterator();
		while(iter.hasNext())
		{
			try
			{
				VOPeliculaPlan actual = iter.next();
				if(actual.getTeatro().getNombre().equals(teatro.getNombre()))
				{
					return actual;
				}
			}
			catch(Exception e)
			{

			}
		}
		return null;
	}


	private EncadenamientoSeparadoTH<Integer, ListaEncadenada<VOPeliculaPlan>> crearTablaFuncionesHora(ListaEncadenada<VOPeliculaPlan> lista)
	{
		EncadenamientoSeparadoTH<Integer, ListaEncadenada<VOPeliculaPlan>> tabla = new EncadenamientoSeparadoTH<>(lista.darNumeroElementos()*2);
		try
		{
			lista = (ListaEncadenada<VOPeliculaPlan>) Merge.sort(lista,new ComparadorHorasFunciones());
		}
		catch(Exception e)
		{

		}
		Iterator<VOPeliculaPlan> iter= lista.iterator();
		while(iter.hasNext())
		{
			VOPeliculaPlan actual = iter.next();
			int hora = actual.getHoraInicio().getTime().getHours();
			if(tabla.darValor(hora) == null)
			{
				tabla.insertar(hora, new ListaEncadenada<>());
			}
			tabla.darValor(hora).agregarAlInicio(actual);
		}
		return tabla;
	}

}
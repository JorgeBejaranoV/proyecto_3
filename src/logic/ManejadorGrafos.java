package logic;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import VOS.VOTeatro;
import VOS.VOTiempo;
import data_structures.DijkstraAlgorithm;
import data_structures.Edge;
import data_structures.Graph;
import data_structures.ListaEncadenada;
import data_structures.Vertex;

public class ManejadorGrafos 
{
	//-----------------------------------------------------------------------------
	// ATRIBUTOS
	//-----------------------------------------------------------------------------

	/**
	 * Grafo para la franja de 10am a 12m
	 */
	private Graph<VOTeatro> grafoMa�ana;

	/**
	 * Grafo para la franja de 12m a 6 pm
	 */
	private Graph<VOTeatro> grafoTarde;

	/**
	 * Grafo para la franja de 6pm a 12pm
	 */
	private Graph<VOTeatro> grafoNoche;

	/***
	 * Lista de los teatros
	 */
	private ListaEncadenada<VOTeatro> listaTeatros;

	public ListaEncadenada<VOTeatro> getListaTeatros() {
		return listaTeatros;
	}

	/**
	 * Lista de tiempos
	 */
	private ListaEncadenada<VOTiempo> listaTiempos;


	/**
	 * Lista auxiliar que guarda los v�rtices
	 */
	private ListaEncadenada<Vertex<VOTeatro>> listaVertices;

	//	/**
	//	 * Lista de los v�rtices de los grafos
	//	 */
	//	private ListaEncadenada<Vertex<VOTeatro>> listaVerticesTeatros;

	/**
	 * Constante que modela la franja de la ma�ana
	 */
	public static final String FRANJA_MA�ANA = "Ma�ana";

	/**
	 * Constante que modela la franja de la tarde
	 */
	public static final String FRANJA_TARDE = "Tarde";

	/**
	 * Constante que modela la franja de la noche
	 */
	public static final String FRANJA_NOCHE = "Noche";

	//-----------------------------------------------------------------------------
	// M�TODOS
	//-----------------------------------------------------------------------------

	/**
	 * Constructor de la clase que maneja los grafos
	 * Se inicializan los grafos
	 */
	public ManejadorGrafos()
	{
		grafoMa�ana = new Graph<VOTeatro>();
		grafoTarde = new Graph<VOTeatro>();
		grafoNoche = new Graph<VOTeatro>();
	}

	/**
	 * M�todo que retorna el grafo de la ma�ana
	 * @return GrafoMa�ana
	 */
	public Graph<VOTeatro> getGrafoMa�ana() 
	{
		return grafoMa�ana;
	}

	/**
	 * M�todo que retorna el grafo de la tarde
	 * @return grafoTarde
	 */
	public Graph<VOTeatro> getGrafoTarde() 
	{
		return grafoTarde;
	}

	/**
	 * M�todo que retorna el grafo de la noche
	 * @return grafoNoche
	 */
	public Graph<VOTeatro> getGrafoNoche() 
	{
		return grafoNoche;
	}

	/**
	 * REQUERIMIENTO 1
	 * M�todo que establece la lista de teatros en la clase
	 * @param listaTeatros
	 */
	public void setListaTeatros(ListaEncadenada<VOTeatro> listaTeatros)
	{
		this.listaTeatros = listaTeatros;
	}

	/**
	 * REQUERIMIENTO 1
	 * M�todo encargado de insertar los teatros a los grafos como v�rtices
	 */
	public boolean insertarTeatrosAGrafos()
	{
		System.out.println("**Insertando teatros a grafos**");
		listaVertices = new ListaEncadenada<Vertex<VOTeatro>>();
		Iterator<VOTeatro> iterator = listaTeatros.iterator();
		int contador = 0;
		while(iterator.hasNext())
		{
			VOTeatro actual = iterator.next();

			//Se inserta el v�rtice en los tres grafos
			Vertex<VOTeatro> v = new Vertex<VOTeatro>(actual.getNombre(), actual); 
			grafoMa�ana.addVertex(v);
			grafoTarde.addVertex(v);
			grafoNoche.addVertex(v);
			listaVertices.agregarAlInicio(v);
			contador++;
		}
		System.out.println("Se han cargado " + contador + " teatros al sistema");
		System.out.println();
		
		boolean b1 = grafoMa�ana.getVerticies().size() != 0;
		boolean b2 = grafoTarde.getVerticies().size() != 0;
		boolean b3 = grafoNoche.getVerticies().size() != 0;
		
		return b1 && b2 && b3;
	}

	/**
	 * REQUERIMIENTO 3
	 * M�todo que establece la lista de tiempos en la clase
	 * @param listaTiempos
	 */
	public void setListaTiempos(ListaEncadenada<VOTiempo> listaTiempos)
	{
		this.listaTiempos = listaTiempos;
	}

	/**
	 * M�todo encargado de insertar los arcos a los grafos
	 */
	public boolean insertarArcosAGrafos()
	{
		System.out.println("**Insertando arcos a los grafos**");
		Iterator<VOTiempo> iter = listaTiempos.iterator();
		int contador = 0;
		while(iter.hasNext())
		{
			try
			{
				//Calculo el peso de cada franja
				VOTiempo actual = iter.next();
				int tiempoMa�ana = actual.getTiempo();
				int tiempoTarde = (int)(tiempoMa�ana*0.15) + tiempoMa�ana;
				int tiempoNoche = (int)(tiempoMa�ana*0.2) + tiempoMa�ana;

				Vertex<VOTeatro> origen = buscarVertice(actual.getOrigen());
				Vertex<VOTeatro> destino = buscarVertice(actual.getDestino());

				grafoMa�ana.insertBiEdge(origen, destino, tiempoMa�ana);
				grafoTarde.insertBiEdge(origen, destino, tiempoTarde);
				grafoNoche.insertBiEdge(origen, destino, tiempoNoche);
				contador++;
			}
			catch(Exception e)
			{

			}

		}
		System.out.println("Se han cargado " + contador + " conexiones al sistema");
		System.out.println();
		
		boolean b1 = grafoMa�ana.getEdges().size() != 0;
		boolean b2 = grafoTarde.getEdges().size() != 0;
		boolean b3 = grafoNoche.getEdges().size() != 0;
		
		return b1 && b2 && b3;
	}

	public Vertex<VOTeatro> buscarVertice(String nombre)
	{
		Iterator<Vertex<VOTeatro>> iter = listaVertices.iterator();
		while(iter.hasNext())
		{
			Vertex<VOTeatro> actual = iter.next();
			if(actual.getName().equals(nombre))
			{
				return actual;
			}
		}
		return null;
	}
	
	private void verticiesToConsole(String franja)
	{
		if(franja.equals(FRANJA_MA�ANA))
		{
			System.out.println(grafoMa�ana.toString());
		}
		else if(franja.equals(FRANJA_TARDE))
		{
			System.out.println(grafoTarde.toString());
		}
		else if(franja.equals(FRANJA_NOCHE))
		{
			System.out.println(grafoNoche.toString());
		}
	}
	
	/**
	 * Calcula el camino m�s corto desde un teatro a otro
	 * @param franja horario del recorrido
	 * @param origen Nombre del teatro de origen
	 * @param destino Nombre del teatri de destino
	 * @return ListaEncadenada con los v�rtices
	 */
	public ListaEncadenada<Vertex<VOTeatro>> caminoMasCorto(String franja, String origen, String destino)
	{
		Vertex<VOTeatro> from = buscarVertice(origen);
		Vertex<VOTeatro> to = buscarVertice(destino);
		ListaEncadenada<Vertex<VOTeatro>> respuesta = new ListaEncadenada<>();
		LinkedList<Vertex<VOTeatro>> listaVert = new LinkedList<>();
		if(franja.equals(FRANJA_MA�ANA))
		{
			listaVert = grafoMa�ana.shortestPath(from, to);
		}
		else if(franja.equals(FRANJA_TARDE))
		{
			listaVert = grafoTarde.shortestPath(from, to);
		}
		else
		{
			listaVert = grafoNoche.shortestPath(from, to);
		}
		
		Iterator<Vertex<VOTeatro>> iter = listaVert.iterator();
		while(iter.hasNext())
		{
			Vertex<VOTeatro> current = iter.next();
			respuesta.agregarAlInicio(current);
		}
		return respuesta;
	}
	
	public double darCosto(String franja, String origen, String destino)
	{
		Vertex<VOTeatro> from = buscarVertice(origen);
		Vertex<VOTeatro> to = buscarVertice(destino);
		double respuesta = 0;
		if(franja.equals(FRANJA_MA�ANA))
		{
			respuesta = grafoMa�ana.getCost(from, to);
		}
		else if(franja.equals(FRANJA_TARDE))
		{
			respuesta = grafoTarde.getCost(from, to);
		}
		else
		{
			respuesta = grafoTarde.getCost(from, to);
		}
		return respuesta;
	}
	
	/**
	 * M�todo que carga el mst de un grafo en una franja espec�fica
	 * @param franja Franja de hora
	 * @return Lista con los arcos a recorrer
	 */
	public List<Edge<VOTeatro>> mst(String franja)
	{
		if(franja.equals(FRANJA_MA�ANA))
		{
			return grafoMa�ana.MST();
		}
		else if(franja.equals(FRANJA_TARDE))
		{
			return grafoTarde.MST();
		}
		else
		{
			return grafoNoche.MST();
		}
	}

}

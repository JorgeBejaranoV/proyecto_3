package logic;

import auxiliaresCarga.DataMatriz;

public class AuxiliarBusquedaSimilitud
{
	private DataMatriz data;
	private String idMovie;
	private double similitud;
	
	public AuxiliarBusquedaSimilitud(DataMatriz data, int idMovie)
	{
		this.data = data;
		this.idMovie = ""+idMovie;
		buscarSimilitud();
	}
	
	public void buscarSimilitud()
	{
		//
		if(idMovie.equals("493405"))
		{
			String sim = data.get_493405();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("498381"))
		{
			String sim = data.get_498381();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("1219827"))
		{
			String sim = data.get_1219827();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("1293847"))
		{
			String sim = data.get_1293847();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("1691916"))
		{
			String sim = data.get_1691916();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("1730768"))
		{
			String sim = data.get_1730768();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("1753383"))
		{
			String sim = data.get_1753383();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("2072233"))
		{
			String sim = data.get_2072233();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("2398241"))
		{
			String sim = data.get_2398241();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("2568862"))
		{
			String sim = data.get_2568862();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("2582576"))
		{
			String sim = data.get_2582576();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("2763304"))
		{
			String sim = data.get_2763304();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("2771200"))
		{
			String sim = data.get_2771200();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("3171832"))
		{
			String sim = data.get_3171832();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("3315342"))
		{
			String sim = data.get_3315342();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("3401882"))
		{
			String sim = data.get_3401882();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("3405236"))
		{
			String sim = data.get_3405236();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("3462710"))
		{
			String sim = data.get_3462710();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("3717490"))
		{
			String sim = data.get_3717490();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("3731562"))
		{
			String sim = data.get_3731562();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("3874544"))
		{
			String sim = data.get_3874544();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("3896198"))
		{
			String sim = data.get_3896198();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("3922818"))
		{
			String sim = data.get_3922818();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("4030600"))
		{
			String sim = data.get_4030600();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("4116284"))
		{
			String sim = data.get_4116284();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("4217392"))
		{
			String sim = data.get_4217392();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("4287320"))
		{
			String sim = data.get_4287320();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("4425200"))
		{
			String sim = data.get_4425200();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("4465564"))
		{
			String sim = data.get_4465564();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("4481414"))
		{
			String sim = data.get_4481414();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("4581576"))
		{
			String sim = data.get_4581576();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("4630562"))
		{
			String sim = data.get_4630562();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("4849438"))
		{
			String sim = data.get_4849438();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("5052448"))
		{
			String sim = data.get_5052448();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("5155780"))
		{
			String sim = data.get_5155780();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("5442430"))
		{
			String sim = data.get_5442430();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("5460276"))
		{
			String sim = data.get_5460276();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("5607714"))
		{
			String sim = data.get_5607714();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("5710514"))
		{
			String sim = data.get_5710514();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		//
		else if(idMovie.equals("5982852"))
		{
			String sim = data.get_5982852();
			if(sim.equals("NaN"))
			{
				similitud = 0;
			}
			else
			{
				similitud = Double.parseDouble(sim);
			}
		}
		else
		{
			similitud = 0;
		}
	}
	
	public double darSimilitud()
	{
		return similitud;
	}
}

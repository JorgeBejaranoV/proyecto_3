package logic;

import java.util.Iterator;

import API.ILista;
import VOS.VOFranquicia;
import VOS.VOGeneroPelicula;
import VOS.VOPelicula;
import VOS.VOPeliculaPlan;
import VOS.VOTeatro;
import data_structures.EncadenamientoSeparadoTH;
import data_structures.HashTable;
import data_structures.ListaEncadenada;
import data_structures.Queue;
import data_structures.RedBlackBST;
import data_structures.Vertex;

/**
 * Clase principal de la aplicaci�n
 */
public class ManejadorPrincipal 
{
	//-----------------------------------------------------------------------------
	// ATRIBUTOS
	//-----------------------------------------------------------------------------

	/**
	 * Manejador de los grafos
	 */
	private ManejadorGrafos manejadorGrafos;

	/**
	 * Manejador de los lectores de archivos
	 */
	private ManejadorLecturas manejadorLecturas;

	/***
	 * Manejador del cat�logo de pel�culas
	 */
	private ManejadorPeliculas manejadorPeliculas;

	/**
	 * Manejador de la carga de la informaci�n
	 */
	private ManejadorCartelera manejadorCartelera;

	/**
	 * Manejador de las similitudes entre pel�culas
	 */
	private ManejadorSimilitudes manejadorSimilitudes;

	/**
	 * Manejador de los usuarios y ratings
	 */
	private ManejadorUsuarios manejadorUsuarios;
	
	/**
	 * Calculador de la agenda
	 */
	private CalculadorAgenda calculadorAgenda;
	
	//-----------------------------------------------------------------------------
	// M�TODOS
	//-----------------------------------------------------------------------------

	/**
	 * Constructor de la clase principal del programa
	 */
	public ManejadorPrincipal()
	{
		manejadorLecturas = new ManejadorLecturas();
		manejadorGrafos = new ManejadorGrafos();
		manejadorSimilitudes = new ManejadorSimilitudes();
	}

	/**
	 * REQUERIMIENTO 1
	 * M�todo encargado de cargar la informaci�n de los teatros en el programa
	 * @return true si se carg�, false de lo contrario
	 */
	public boolean cargarTeatros()
	{
		boolean r = manejadorLecturas.leerTeatros();
		manejadorGrafos.setListaTeatros(manejadorLecturas.darListaTeatros());
		return r;
	}

	/**
	 * REQUERIMIENTO 2
	 * M�todo encargado de cargar la cartelera al sistema
	 * @return true si se carg�, false de lo contrario
	 */
	public boolean cargarCartelera()
	{	
		manejadorLecturas.leerPeliculas();
		manejadorPeliculas = new ManejadorPeliculas(manejadorLecturas.darListaPeliculas());
		boolean r = manejadorLecturas.leerFuncionesDias();
		manejadorCartelera = new ManejadorCartelera(manejadorPeliculas, manejadorLecturas.darListaTeatros(), manejadorLecturas.darFuncionesDias());
		return r;
	}

	/**
	 * REQUERIMIENTO 3
	 * M�todo encargado de crear la red de teatros y pel�culas
	 * @return true si se carg�, false de lo contrario
	 */
	public boolean cargarRed()
	{

		manejadorLecturas.leerTiempos();
		manejadorGrafos.setListaTiempos(manejadorLecturas.darListaTiempos());
		boolean r1 = manejadorGrafos.insertarTeatrosAGrafos();
		boolean r2 = manejadorGrafos.insertarArcosAGrafos();
		return r1 && r2;
	}


	/**
	 * Retorna el n�mero de pel�culas
	 * @return n�mero de pel�culas
	 */
	public int sizeMovies()
	{
		return manejadorPeliculas.sizeMovies();
	}

	/**
	 * M�todo que crea el manejador de usuarios
	 */
	public void cargarManejadorUsuarios()
	{
		//Se leen los ratings para crear el manejador de usuarios
		manejadorLecturas.leerRatings();
		manejadorUsuarios = new ManejadorUsuarios(manejadorLecturas.darListaRatings());
	}
	
	/**
	 * Crea el calculador de agenda
	 */
	public void crearCalculadorAgenda()
	{
		calculadorAgenda = new CalculadorAgenda(manejadorCartelera, manejadorPeliculas, manejadorSimilitudes, manejadorGrafos, manejadorUsuarios);
	}
	
	/**
	 * REQUERIMIENTO 7
	 * M�todo que calcula la agenda de las pel�culas de un g�nero en un d�a
	 * @param genero
	 * @param dia
	 * @return Lista con el plan
	 */
	public void planPorGeneroYDesplazamiento(String genero, int dia)
	{
		Queue<VOPeliculaPlan> l = calculadorAgenda.planPorGeneroYDesplazamiento(genero, dia);
		while(!l.isEmpty())
		{
			VOPeliculaPlan actual = l.dequeue();
			VOTeatro n = new VOTeatro();
			n.setNombre("Cine Colombia Iserra 100");
			if(actual.getTeatro() == null)
			{
				actual.setTeatro(n);
			}
			System.out.println(actual.getTeatro().getNombre());
			System.out.println(actual.getPelicula().getTitulo());
			System.out.println();
		}
	}
	public ManejadorGrafos getManejadorGrafos() {
		return manejadorGrafos;
	}

	public void setManejadorGrafos(ManejadorGrafos manejadorGrafos) {
		this.manejadorGrafos = manejadorGrafos;
	}

	public ManejadorLecturas getManejadorLecturas() {
		return manejadorLecturas;
	}

	public void setManejadorLecturas(ManejadorLecturas manejadorLecturas) {
		this.manejadorLecturas = manejadorLecturas;
	}

	public ManejadorPeliculas getManejadorPeliculas() {
		return manejadorPeliculas;
	}

	public void setManejadorPeliculas(ManejadorPeliculas manejadorPeliculas) {
		this.manejadorPeliculas = manejadorPeliculas;
	}

	public ManejadorCartelera getManejadorCarga() {
		return manejadorCartelera;
	}

	public void setManejadorCarga(ManejadorCartelera manejadorCarga) {
		this.manejadorCartelera = manejadorCarga;
	}
	public ListaEncadenada<String> getListaRecomendaciones(){
		ListaEncadenada<String> listaReco= new ListaEncadenada<>();
		listaReco.agregarAlInicio("Star Wars Episode I: The Phantom Menace");
		listaReco.agregarAlInicio("Star Wars Episode II: Attack of the clones");
		listaReco.agregarAlInicio("Star Wars Episode III: Revenge of the sith");
		listaReco.agregarAlInicio("Star Wars Episode IV: A New Hope");
		listaReco.agregarAlInicio("Star Wars Episode V: Empire Strikes Back");
		listaReco.agregarAlInicio("Star Wars Episode VI: Return of the jedi");
		listaReco.agregarAlInicio("Star Wars Episode VII: The Force Awakens");
		return listaReco;
	}
	public ListaEncadenada<String> getListaRecomendaciones(int idUsuario){
		ListaEncadenada<String> listaReco= new ListaEncadenada<>();
		listaReco.agregarAlInicio("Star Wars Episode I: The Phantom Menace");
		listaReco.agregarAlInicio("Star Wars Episode II: Attack of the clones");
		listaReco.agregarAlInicio("Star Wars Episode III: Revenge of the sith");
		listaReco.agregarAlInicio("Star Wars Episode IV: A New Hope");
		listaReco.agregarAlInicio("Star Wars Episode V: Empire Strikes Back");
		listaReco.agregarAlInicio("Star Wars Episode VI: Return of the jedi");
		listaReco.agregarAlInicio("Star Wars Episode VII: The Force Awakens");
		return listaReco;
	}
	/**
	 * M�todo que retorna la tabla de planes
	 * @return tabla de hash con planes
	 */
	

	//Prueba
	public static void main(String[] args)
	{
		ManejadorPrincipal m = new ManejadorPrincipal();
		m.cargarTeatros();
		m.cargarCartelera();
		m.cargarRed();
		m.cargarManejadorUsuarios();
		m.crearCalculadorAgenda();
		VOFranquicia nueva=new VOFranquicia();
		nueva.setNombre("Cine Colombia");
		
	}
	
	/**R6
	 * Genera un plan de peliculas en la fecha dada pasando solo por teatros de una misma franquicia
	 * maximizando el numero de pliculas
	 * El usuario puede (o no) precisar una franja de horario (ej. ma�ana y noche)
	 * @param franquicia
	 * @param fecha (1..5)
	 * @param franja
	 * @return plan propuesto
	 */

public ListaEncadenada<VOPeliculaPlan> PlanPorFranquicia(String franqu, int fecha,String franja){


		// INICIALIZACION DE LA RESPUESTA
		ListaEncadenada<VOPeliculaPlan> lista = new ListaEncadenada<VOPeliculaPlan>();


		//INFO DE LAS PEL�CULAS
		EncadenamientoSeparadoTH<String, RedBlackBST<Long, ListaEncadenada<VOPeliculaPlan>>>[] peliculasTabla =manejadorCartelera.darTablaFunciones2();


		//INFO DE PARAMETROS
		



		//GENERADOR DE GRAFOS
		ListaEncadenada<ListaEncadenada<Vertex<VOTeatro>>> laListota = new ListaEncadenada<ListaEncadenada<Vertex<VOTeatro>>>();


		//ANALIZADOR DE CAMNIO M�S CORTO ENTRE GRAFOS EN LA MA�ANA

		Iterator<String> llaves1 =peliculasTabla[fecha].keys().iterator();
		int mayorNodos =0;
		String teatroSeguir = "Cine Colombia Cedritos";

		String teatro1= "";
		while (llaves1.hasNext()){

			teatro1 = llaves1.next();



			try{manejadorGrafos.caminoMasCorto("Ma�ana", teatroSeguir, teatro1);}catch(Exception e){continue;

			}

			laListota.agregarElementoFinal(manejadorGrafos.caminoMasCorto("Ma�ana", teatroSeguir, teatro1));
			teatroSeguir = teatro1;


		}



		//FILTRACI�N DE LAS PEL�CULAS DEBIDO A G�NERO Y FRANQUICIA

		for(int i = 0; i< laListota.darNumeroElementos(); i++){
			ListaEncadenada<Vertex<VOTeatro>> info = laListota.darElemento(i);

			for(int y = 0; y< info.darNumeroElementos(); y++){
				Vertex<VOTeatro> subTeatro = info.darElemento(y);

				VOTeatro subTeatro1 = subTeatro.getData();



				//se obtiene la informaci�n de el vetrice escogido
				// fra��nquicia
				String subFranquicia = subTeatro1.getFranquicia().getNombre();

				// nombre del teatro
				String subTeatro2 = subTeatro1.getNombre();

				// se inicializa un objeto de la respuesta
				VOPeliculaPlan respuestaTotal = new VOPeliculaPlan();

				// se establece el nombre del teatro y el d�a escogido

				respuestaTotal.setTeatro(subTeatro1);
				respuestaTotal.setDia(fecha);



				// si el teatro  pertenece a la franquicia por param
				if(subFranquicia.equals(franqu)){

					// se busca el teatro y se da la info de las pel�culas
					RedBlackBST<Long, ListaEncadenada<VOPeliculaPlan>> elegida = peliculasTabla[fecha].darValor(subTeatro2);

					//se da los id de las pel�culas
					ILista<Long> llaves =elegida.darLlavesIntervalo(elegida.min(), elegida.max());

					// se empiezan a recorrer todas las pel�culas

					for(int k = 0 ; k<llaves.darNumeroElementos(); k++){

						// se tiene el id de la pel�cula
						VOPelicula peliRespuesta = new VOPelicula(0	, "", null);


						peliRespuesta.setId(llaves.darElemento(k));



						// se analiza cada pel�cula
						ListaEncadenada<VOPeliculaPlan> infoPeli = elegida.get(llaves.darElemento(k));

						//se tiene una variable de comparaci�n
						int max =0;

						for(int j= 0; j < infoPeli.darNumeroElementos() && infoPeli.darElemento(j).getDia() ==fecha; j++){

							int duracion = infoPeli.darElemento(j).getHoraFin().getTime().getHours();
							if( franja.equals("") ){

								// solo se busca la pel�cula m�s temprana
								
									System.out.println("++++++++++++++");
									//se cambia el max si es la m�s temprana 
									max = duracion;

									// se agrega la pel�cula
									peliRespuesta.setTitulo(infoPeli.darElemento(j).getPelicula().getTitulo());						
									respuestaTotal.setHoraInicio(infoPeli.darElemento(j).getHoraInicio());
									respuestaTotal.setHoraFin(infoPeli.darElemento(j).getHoraFin());
								
							}
							else if(franja.equals("Ma�ana")){


								if(duracion > 12&& duracion>10 ){
									System.out.println("+++");
									//se cambia el max si es la m�s temprana 
									max = duracion;

									// se agrega la pel�cula
									peliRespuesta.setTitulo(infoPeli.darElemento(j).getPelicula().getTitulo());	

									respuestaTotal.setHoraInicio(infoPeli.darElemento(j).getHoraInicio());
									respuestaTotal.setHoraFin(infoPeli.darElemento(j).getHoraFin());

								}

							}
							else if(franja.equals("Tarde")){
								if(duracion > 12&& duracion>6){
									System.out.println("---------");
									//se cambia el max si es la m�s temprana 
									max = duracion;

									// se agrega la pel�cula
									peliRespuesta.setTitulo(infoPeli.darElemento(j).getPelicula().getTitulo());						
									respuestaTotal.setHoraInicio(infoPeli.darElemento(j).getHoraInicio());
									respuestaTotal.setHoraFin(infoPeli.darElemento(j).getHoraFin());
								}

							}

							else if(franja.equals("Noche")){

								if(duracion > 6&& duracion>10){
									System.out.println("-------------");
									//se cambia el max si es la m�s temprana 
									max = duracion;

									// se agrega la pel�cula
									peliRespuesta.setTitulo(infoPeli.darElemento(j).getPelicula().getTitulo());						
									respuestaTotal.setHoraInicio(infoPeli.darElemento(j).getHoraInicio());
									respuestaTotal.setHoraFin(infoPeli.darElemento(j).getHoraFin());
								}
							}

						}



						//se termina de completar la informaci�n del VOPeliculaPlan
					
						respuestaTotal.setPelicula(peliRespuesta);

					}



				
					lista.agregarElementoFinal(respuestaTotal);


				}
			}
		}

		return lista;

	}

	/**
	 * REQUERIMIENTO 10
	 */

	public ListaEncadenada<ListaEncadenada<Vertex<VOTeatro>>> rutasPosible(VOTeatro origen, int n){




		//INFO DE LAS PEL�CULAS
		EncadenamientoSeparadoTH<String, RedBlackBST<Long, ListaEncadenada<VOPeliculaPlan>>>[] peliculasTabla =manejadorCartelera.darTablaFunciones2();


		//RESPUESTA
		ListaEncadenada<ListaEncadenada<Vertex<VOTeatro>>> respuesta =new ListaEncadenada<ListaEncadenada<Vertex<VOTeatro>>>();

		//GENERADOR DE GRAFOS
		ListaEncadenada<ListaEncadenada<Vertex<VOTeatro>>> laListota = new ListaEncadenada<ListaEncadenada<Vertex<VOTeatro>>>();


		//ANALIZADOR DE CAMNIO M�S CORTO ENTRE GRAFOS EN LA MA�ANA

		Iterator<String> llaves1 =peliculasTabla[1].keys().iterator();
		String teatroSeguir = origen.getNombre();

		String teatro1= "";
		while (llaves1.hasNext()){

			teatro1 = llaves1.next();



			try{manejadorGrafos.caminoMasCorto("Ma�ana", teatroSeguir, teatro1);}catch(Exception e){continue;

			}
			laListota.agregarElementoFinal(manejadorGrafos.caminoMasCorto("Ma�ana", teatroSeguir, teatro1));

		}
		for(int i = 0; i< laListota.darNumeroElementos(); i++){

			if(laListota.darElemento(i).darNumeroElementos()<=n){


				respuesta.agregarElementoFinal(laListota.darElemento(i));
			}

		}
		return respuesta;
	}



	/**
	 * REQUERIMIETNO 8
	 * @param genero
	 * @param fecha
	 * @param franquicia
	 * @return
	 */
	public ListaEncadenada<VOPeliculaPlan> planPorGeneroDesplazamientoYFranquicia(String genero, int fecha, String franqu){


		// INICIALIZACION DE LA RESPUESTA
		ListaEncadenada<VOPeliculaPlan> lista = new ListaEncadenada<VOPeliculaPlan>();


		//INFO DE LAS PEL�CULAS
		EncadenamientoSeparadoTH<String, RedBlackBST<Long, ListaEncadenada<VOPeliculaPlan>>>[] peliculasTabla =manejadorCartelera.darTablaFunciones2();


		//INFO DE PARAMETROS

		



		//GENERADOR DE GRAFOS
		ListaEncadenada<ListaEncadenada<Vertex<VOTeatro>>> laListota = new ListaEncadenada<ListaEncadenada<Vertex<VOTeatro>>>();


		//ANALIZADOR DE CAMNIO M�S CORTO ENTRE GRAFOS EN LA MA�ANA

		Iterator<String> llaves1 =peliculasTabla[fecha].keys().iterator();
		int mayorNodos =0;
		String teatroSeguir = "Cine Colombia Cedritos";

		String teatro1= "";
		while (llaves1.hasNext()){

			teatro1 = llaves1.next();



			try{manejadorGrafos.caminoMasCorto("Ma�ana", teatroSeguir, teatro1);}catch(Exception e){continue;

			}

			laListota.agregarElementoFinal(manejadorGrafos.caminoMasCorto("Ma�ana", teatroSeguir, teatro1));
			teatroSeguir = teatro1;


		}



		//FILTRACI�N DE LAS PEL�CULAS DEBIDO A G�NERO Y FRANQUICIA

		for(int i = 0; i< laListota.darNumeroElementos(); i++){
			ListaEncadenada<Vertex<VOTeatro>> info = laListota.darElemento(i);

			for(int y = 0; y< info.darNumeroElementos(); y++){
				Vertex<VOTeatro> subTeatro = info.darElemento(y);

				VOTeatro subTeatro1 = subTeatro.getData();



				//se obtiene la informaci�n de el vetrice escogido
				// fra��nquicia
				String subFranquicia = subTeatro1.getFranquicia().getNombre();

				// nombre del teatro
				String subTeatro2 = subTeatro1.getNombre();

				// se inicializa un objeto de la respuesta
				VOPeliculaPlan respuestaTotal = new VOPeliculaPlan();

				// se establece el nombre del teatro y el d�a escogido

				respuestaTotal.setTeatro(subTeatro1);
				respuestaTotal.setDia(fecha);



				// si el teatro  pertenece a la franquicia por param
				if(subFranquicia.equals(franqu)){

					// se busca el teatro y se da la info de las pel�culas
					RedBlackBST<Long, ListaEncadenada<VOPeliculaPlan>> elegida = peliculasTabla[fecha].darValor(subTeatro2);

					//se da los id de las pel�culas
					ILista<Long> llaves =elegida.darLlavesIntervalo(elegida.min(), elegida.max());

					// se empiezan a recorrer todas las pel�culas

					for(int k = 0 ; k<llaves.darNumeroElementos(); k++){

						// se tiene el id de la pel�cula
						VOPelicula peliRespuesta = new VOPelicula(0	, "", null);

						System.out.println(llaves.darElemento(k));
						peliRespuesta.setId(llaves.darElemento(k));



						// se analiza cada pel�cula
						ListaEncadenada<VOPeliculaPlan> infoPeli = elegida.get(llaves.darElemento(k));

						//se tiene una variable de comparaci�n
						int max =0;

						for(int j= 0; j < infoPeli.darNumeroElementos() && infoPeli.darElemento(j).getDia() ==fecha; j++){

							int duracion = infoPeli.darElemento(j).getHoraInicio().getTime().getHours();
							System.out.println(duracion);
							// solo se busca la pel�cula m�s temprana
							if(duracion > max&& j ==0 ){

								//se cambia el max si es la m�s temprana 
								max = duracion;

								// se agrega la pel�cula
								peliRespuesta.setTitulo(infoPeli.darElemento(j).getPelicula().getTitulo());



								// se compara con la lista de g�neros que tiene una pel�cula
								boolean x = false;
								for(int l = 0; l< infoPeli.darElemento(j).getPelicula().getGeneros().length && !x ; l++){

									// se filtran las pel�culas por g�nero dado en el param


									if(infoPeli.darElemento(j).getPelicula().getGeneros()[l].equals(genero)){

										// si existe por lo menos un g�nero que cumpla con la condici�n , se permite aceptar todos los g�neros
										peliRespuesta.setGeneros(infoPeli.darElemento(j).getPelicula().getGeneros());

										// se establece hora inicio y hora final

										respuestaTotal.setHoraInicio(infoPeli.darElemento(j).getHoraInicio());
										respuestaTotal.setHoraFin(infoPeli.darElemento(j).getHoraFin());

										x = true;

									}

								}							

							}



						}

						if(peliRespuesta != null  &&  peliRespuesta.getGeneros() != null &&  peliRespuesta.getTitulo()!= null ){

							//se termina de completar la informaci�n del VOPeliculaPlan

							respuestaTotal.setPelicula(peliRespuesta);

						}


					}

					if(respuestaTotal != null&& respuestaTotal.getPelicula()!= null){


						lista.agregarElementoFinal(respuestaTotal);
					}

				}


			}
		}



		return lista;
	}

}

package logic;

import VOS.VOPelicula;
import VOS.VORating;
import VOS.VOTeatro;
import VOS.VOTiempo;
import auxiliaresCarga.DataFuncion;
import auxiliaresCarga.DataRequest;
import data_structures.ListaEncadenada;
import lectores.LectorFuncionesDias;
import lectores.LectorPeliculas;
import lectores.LectorRatings;
import lectores.LectorRequests;
import lectores.LectorTeatros;
import lectores.LectorTiempos;

/**
 * Clase encargada de manejar los diferentes lectores de archivos
 */
public class ManejadorLecturas 
{
	//-----------------------------------------------------------------------------
	// ATRIBUTOS
	//-----------------------------------------------------------------------------
	
	/**
	 * Lector de peliculas filtradas
	 */
	private LectorPeliculas lectorPeliculas;
	
	/**
	 * Lector de archivo de teatros
	 */
	private LectorTeatros lectorTeatros;
	
	/**
	 * Lector del archivo de tiemposDistancias
	 */
	private LectorTiempos lectorTiempos;
	
	/**
	 * Lector del archivo de funciones
	 */
	private LectorFuncionesDias lectorFuncionesDias;
	
	/**
	 * Lector del archivo de ratings
	 */
	private LectorRatings lectorRatings;
	
	/**
	 * Lista de teatros
	 */
	private ListaEncadenada<VOTeatro> listaTeatros;
	
	/**
	 * Arreglo de las funciones por d�a
	 */
	private ListaEncadenada<DataFuncion>[] funcionesDias;
	
	/**
	 * Lista de pel�culas
	 */
	private ListaEncadenada<VOPelicula> listaPeliculas;
	
	/**
	 * Lista con los tiempos de recorrido
	 */
	private ListaEncadenada<VOTiempo> listaTiempos;
	
	/**
	 * Lista con los ratings
	 */
	private ListaEncadenada<VORating> listaRatings;
	
	//-----------------------------------------------------------------------------
	// M�TODOS
	//-----------------------------------------------------------------------------
	
	/**
	 * Constructor de la clase ManejadorLecturas
	 * Se encarga de inicializar todos los lectores
	 */
	public ManejadorLecturas()
	{
		lectorTeatros = new LectorTeatros();
		lectorTiempos = new LectorTiempos();
		lectorFuncionesDias = new LectorFuncionesDias();
		lectorPeliculas = new LectorPeliculas();
		lectorRatings = new LectorRatings();
	}
	
	/**
	 * REQUERIMIENTO 1
	 * M�todo que se encarga de la lectura de los teatros
	 * @return true si la lista tiene elementos, false de lo contrario
	 */
	public boolean leerTeatros()
	{
		this.listaTeatros = lectorTeatros.darTeatros();
		return listaTeatros.darNumeroElementos() != 0;
	}
	
	/**
	 * M�todo que lee las pel�culas
	 * @return true si la lista contiene elementos, false de lo contrario
	 */
	public boolean leerPeliculas()
	{
		this.listaPeliculas = lectorPeliculas.darListaPeliculas();
		return listaPeliculas.darNumeroElementos() != 0;
	}
	
	/**
	 * REQUERIMIENTO 2
	 * M�todo que se encarga de la lectura de la programaci�n
	 * @return true si hay elementos en el arreglo, false de lo contrario
	 */
	public boolean leerFuncionesDias()
	{
		funcionesDias =(ListaEncadenada<DataFuncion>[]) new ListaEncadenada[5];
		funcionesDias[0] = lectorFuncionesDias.listaDia1();
		funcionesDias[1] = lectorFuncionesDias.listaDia2();
		funcionesDias[2] = lectorFuncionesDias.listaDia3();
		funcionesDias[3] = lectorFuncionesDias.listaDia4();
		funcionesDias[4] = lectorFuncionesDias.listaDia5();
		
		boolean cargado = true;
		for(int i = 0;i < funcionesDias.length; i++)
		{
			if(funcionesDias[i] == null)
			{
				cargado = false;
			}
		}
		
		return cargado;
	}
	
	
	/**
	 * REQUERIMIENTO 3
	 * @return true si carg� los elementos, false de lo contrario
	 */
	public boolean leerTiempos()
	{
		this.listaTiempos = lectorTiempos.darTiempos();
		return listaTiempos.darNumeroElementos() != 0;
	}
	
	/**
	 * M�todo que guarda la lista de ratings
	 * @return true si se carg�, false de lo contrario
	 */
	public boolean leerRatings()
	{
		this.listaRatings = lectorRatings.darListaRatings();
		return listaRatings.darNumeroElementos() != 0;
	}
	
	/**
	 * M�todo que retorna la lista de pel�culas cargadas
	 * @return pel�culas cargadas
	 */
	public ListaEncadenada<VOPelicula> darListaPeliculas()
	{
		return listaPeliculas;
	}
	
	/**
	 * M�todo que retorna la lista con los teatros cargas
	 * @return lista con los teatro
	 */
	public ListaEncadenada<VOTeatro> darListaTeatros()
	{
		return listaTeatros;
	}
	
	/**
	 * M�todo que retorna la lista con los tiempos cargados
	 * @return lista de tiempos
	 */
	public ListaEncadenada<VOTiempo> darListaTiempos()
	{
		return listaTiempos;
	}
	
	/**
	 * M�todo que retorna un arreglo con las funciones
	 * @return arreglo de funciones
	 */
	public ListaEncadenada<DataFuncion>[] darFuncionesDias()
	{
		return funcionesDias;
	}
	
	/**
	 * M�todo que retorna la lista de ratings
	 * @return lista de ratings
	 */
	public ListaEncadenada<VORating> darListaRatings()
	{
		return listaRatings;
	}
	
	/**
	 * M�todo que lee la petici�n de un usuario
	 * @return objeto de tipo petici�n
	 */
	public DataRequest leerPeticion(String pRuta)
	{
		LectorRequests lectorRequest = new LectorRequests(pRuta);
		return lectorRequest.darRequest();
	}
	
}

package logic;

import java.util.Iterator;

import VOS.VORating;
import VOS.VOUsuario;
import data_structures.EncadenamientoSeparadoTH;
import data_structures.ListaEncadenada;
import data_structures.MaxHeap;
import data_structures.RedBlackBST;

/**
 * Clase encargada del manejo de los usuarios y los ratings
 */
public class ManejadorUsuarios 
{
	//-----------------------------------------------------------------------------
	// ATRIBUTOS
	//-----------------------------------------------------------------------------

	/**
	 * Lista con todos los ratings
	 */
	private ListaEncadenada<VORating> listaRatings;

	/**
	 * Tabla de hash con los ratings donde la llave es el id del usuario
	 * La llave en el �rbol es el id de la pel�cula
	 */
	private EncadenamientoSeparadoTH<Integer, RedBlackBST<Integer, VORating>> tablaRatings;

	/**
	 * �rbol balanceado con los usuarios donde la llave es el id del usuario
	 */
	private RedBlackBST<Integer, VOUsuario> arbolUsuarios;

	//-----------------------------------------------------------------------------
	// M�TODOS
	//-----------------------------------------------------------------------------

	/**
	 * Constructor del manejador
	 * @param listaRatings lista de los ratings
	 */
	public ManejadorUsuarios(ListaEncadenada<VORating> listaRatings)
	{
		this.listaRatings = listaRatings;
		boolean b = crearEstructuras();
		if(b == false)
		{
			System.out.println("No se cargaron correctamente los datos");
		}
		else
		{
			System.out.println("Se cargaron los datos correctamente");
		}
	}

	/**
	 * M�todo encargado de crear el �rbol de usuarios
	 */
	private boolean crearEstructuras()
	{
		System.out.println("**Cargando usuarios y ratings al sistema**");
		//Inicializa las estructuras
		arbolUsuarios = new RedBlackBST<>();
		tablaRatings = new EncadenamientoSeparadoTH<>(listaRatings.darNumeroElementos()*2);

		//Recorrido por los ratings
		Iterator<VORating> iter = listaRatings.iterator();
		while(iter.hasNext())
		{
			//Crea el usuario y lo agrega al �rbol
			VORating actual = iter.next();
			VOUsuario nuevo = new VOUsuario();
			nuevo.setId(actual.getUserId());
			arbolUsuarios.put(nuevo.getId(), nuevo);


			//Inserta el rating a la tabla
			if(tablaRatings.darValor(actual.getUserId()) == null)
			{
				tablaRatings.insertar(actual.getUserId(), new RedBlackBST<>());
			}
			tablaRatings.darValor(actual.getUserId()).put(actual.getMovieId(), actual);
		}
		if(arbolUsuarios.isEmpty() || tablaRatings.estaVacia())
		{
			return false;
		}
		return true;
	}

	/**
	 * M�todo encargado de buscar un rating espec�fico en el sistema
	 * @param userId id del usuario
	 * @param movieId id de la pel�cula
	 * @return rating, null si no existe
	 */
	public VORating buscarRating(int userId, int movieId)
	{
		return tablaRatings.darValor(userId).get(movieId);
	}

	/**
	 * M�todo que busca a un usuario
	 * @param idUsuario id del usuario
	 * @return usuario buscado, null en caso de no encontrarlo
	 */
	public VOUsuario buscarUsuario(int idUsuario)
	{
		return arbolUsuarios.get(idUsuario);
	}

	/**
	 * Busca todos los ratings que ha hecho un usuario
	 * @param userId id del usuario
	 * @return lista con todos los ratings
	 */
	public MaxHeap<VORating> darRatingsUsuario(int userId)
	{
		MaxHeap<VORating> respuesta = new MaxHeap<>(VORating.class);
		RedBlackBST<Integer, VORating> ratings = tablaRatings.darValor(userId);
		ListaEncadenada<VORating> lista = ratings.treeToList();

		Iterator<VORating> iter = lista.iterator();
		while(iter.hasNext())
		{
			VORating actual = iter.next();
			try 
			{
				respuesta.agregar(actual);
			} 
			catch (Exception e) 
			{

			}
		}

		return respuesta;
	}
}

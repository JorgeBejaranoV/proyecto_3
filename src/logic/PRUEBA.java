package logic;


import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import javax.swing.GrayFilter;

import API.ILista;
import VOS.VOPelicula;
import VOS.VOPeliculaPlan;
import VOS.VOTeatro;
import data_structures.DijkstraAlgorithm;
import data_structures.Edge;
import data_structures.Graph;
import data_structures.HashTable;
import data_structures.Kruskal;
import data_structures.ListaEncadenada;
import data_structures.Queue;
import data_structures.Vertex;


public class PRUEBA {
	private static ManejadorPrincipal m;
	
	
	
	public static ILista<VOPeliculaPlan> planPorFecha(Calendar fechaNecesaria, int idUsuario){
		Graph<VOTeatro> ma�ana=m.getManejadorGrafos().getGrafoMa�ana();
		Graph<VOTeatro> tarde=m.getManejadorGrafos().getGrafoTarde();
		Graph<VOTeatro> noche=m.getManejadorGrafos().getGrafoNoche();
		int contFecha=fechaNecesaria.getTime().getHours();
		ListaEncadenada<String> listaRecomendacionUsuario=m.getListaRecomendaciones(idUsuario); 
		
		ListaEncadenada<VOPelicula> listaPelis= new ListaEncadenada<>();
		//SI SE INICIA DE MA�ANITA
		Vertex<VOTeatro> verticeActual=ma�ana.getVertex(0);
		while(contFecha<12){
			
			String nombreT=verticeActual.getData().getNombre();
			HashTable<String, ListaEncadenada<VOPeliculaPlan>> tabla=m.getManejadorCarga().darTablaFunciones();
			ListaEncadenada<VOPeliculaPlan> listaPeliculasFuncion=tabla.darValor(nombreT);
			for(int i=0;i<listaPeliculasFuncion.darNumeroElementos();i++){
				VOPelicula aAgregar=listaPeliculasFuncion.darElemento(i).getPelicula();
				int id=(int) aAgregar.getId();
				boolean bandera=false;
				
				if(!seAgrego(id,listaPelis)){
				
					for(int j=0;j<verticeActual.getOutgoingEdgeCount();j++){
						
						if(listaPeliculasFuncion.darElemento(i+1)!=null&&(listaPeliculasFuncion.darElemento(i+1).getHoraInicio().getTime().getHours()<contFecha+(verticeActual.getOutgoingEdge(j).getCost()/60)+1.423)){
							contFecha=contFecha+(verticeActual.getOutgoingEdge(j).getCost()/60+2);
							
							verticeActual=verticeActual.getOutgoingEdge(j).getTo();
							if(contFecha<12){
							System.out.println("Nombre de la pel�cula: "+"/n"+aAgregar.getTitulo()+"Teatro: "+nombreT+"/n"+"Hora"+contFecha+":00");}
							bandera=true;
							
							break;
							
						}
					}
					if(bandera){
						break;
					}

					if(!seAgrego(id,listaPelis)){
						
						if(listaPeliculasFuncion.darElemento(i).getHoraInicio().getTime().getHours()<12){
						listaPelis.agregarAlInicio(aAgregar);
						System.out.println("Nombre de la pel�cula: "+aAgregar.getTitulo()+"\n"+"Teatro: "+nombreT+"\n"+"Hora: "+contFecha+":00");
						contFecha+=2;}
					}

					
				}
				
			}
					
			}
		 verticeActual=tarde.getVertex(0);
		while(contFecha>12 && contFecha<18){
			
			String nombreT=verticeActual.getData().getNombre();
			HashTable<String, ListaEncadenada<VOPeliculaPlan>> tabla=m.getManejadorCarga().darTablaFunciones();
			ListaEncadenada<VOPeliculaPlan> listaPeliculasFuncion=tabla.darValor(nombreT);
			for(int i=0;i<listaPeliculasFuncion.darNumeroElementos();i++){
				VOPelicula aAgregar=listaPeliculasFuncion.darElemento(i).getPelicula();
				int id=(int) aAgregar.getId();
				boolean bandera=false;

				if(!seAgrego(id,listaPelis)){
					for(int j=0;j<verticeActual.getOutgoingEdgeCount();j++){

						if(listaPeliculasFuncion.darElemento(i+1)!=null&&(listaPeliculasFuncion.darElemento(i+1).getHoraInicio().getTime().getHours()<contFecha+(verticeActual.getOutgoingEdge(j).getCost()/60)+1.423)){
							contFecha=contFecha+(verticeActual.getOutgoingEdge(j).getCost()/60+2);

							verticeActual=verticeActual.getOutgoingEdge(j).getTo();
							if(contFecha<18){
								System.out.println("Nombre de la pel�cula: "+aAgregar.getTitulo()+"\n"+"Teatro: "+nombreT+"\n"+"Hora"+contFecha+":00");}
							bandera=true;

							break;

						}
					}
					if(bandera){
						break;
					}

					if(!seAgrego(id,listaPelis)){

						if(listaPeliculasFuncion.darElemento(i).getHoraInicio().getTime().getHours()<18){
							listaPelis.agregarAlInicio(aAgregar);
							System.out.println("Nombre de la pel�cula: "+aAgregar.getTitulo()+"\n"+"Teatro: "+nombreT+"\n"+"Hora: "+contFecha+":00");
							contFecha+=2;}
					}


				}


				//DijkstraAlgorithm<VOTeatro> dijkstraManana= new DijkstraAlgorithm<>(manejadorGrafos.getGrafoMa�ana());


		}
	}
		return null;
	}
	
	public static void diaMayorCantidadPeliculas(String genero){
		Queue<VOTeatro> cola = new Queue<>();
		int mayorMayor=0;
		int diaMayor=0;
		for(int a=0;a<5;a++){

			ListaEncadenada<VOTeatro> listaTeatros=m.getManejadorLecturas().darListaTeatros();

			Graph<VOTeatro> ma�ana=m.getManejadorGrafos().getGrafoMa�ana();

			DijkstraAlgorithm<VOTeatro> dijkstra=new DijkstraAlgorithm<>(ma�ana);
			Vertex<VOTeatro> inicio=m.getManejadorGrafos().buscarVertice("Cine Colombia Cedritos");
			dijkstra.execute(inicio);
			int mayor=0;
			LinkedList<Vertex<VOTeatro>> listaMayor=null;
			for(int i=0;i<listaTeatros.darNumeroElementos();i++){
				int numPeliculasGenero=0;
				LinkedList<Vertex<VOTeatro>> listaV=dijkstra.getPath(m.getManejadorGrafos().buscarVertice(listaTeatros.darElemento(i).getNombre()));

				if(listaV==null){
					continue;
				}
				for(int j=0; j<listaV.size();j++){
					String nombreTeatro=listaV.get(j).getData().getNombre();
					numPeliculasGenero+=contarPeliculasGenero(genero, nombreTeatro, ManejadorGrafos.FRANJA_MA�ANA,a);	
				}
				if(numPeliculasGenero>mayor){
					mayor=numPeliculasGenero;
					listaMayor=listaV;
				}	
			}
			for(int i=0;i<listaMayor.size();i++){

				cola.enqueue(listaMayor.get(i).getData());	
			}
			if(mayor>mayorMayor){
				diaMayor=a;
			}
		}

		System.out.println("El d�a con m�s pel�culas del g�nero dado, es el n�mero "+diaMayor);
		System.out.println("Te recomendamos seguir esta gu�a de teatros y pel�culas para aprovechar al m�ximo el festival");
		ListaEncadenada<Integer> auxiliar= new ListaEncadenada<>();
		ListaEncadenada<String> auxiliar2=new ListaEncadenada<>();
		ListaEncadenada<String> auxiliar3=new ListaEncadenada<>();
		while(!cola.isEmpty()){

			HashTable<String, ListaEncadenada<VOPeliculaPlan>> tabla=m.getManejadorCarga().darTablaFunciones();
			String nombreTeatro=cola.dequeue().getNombre();
			int var=19-cola.size()+1;
			System.out.println("**** OPCI�N "+var+" ******");
			ListaEncadenada<VOPeliculaPlan> lista=tabla.darValor(nombreTeatro);

			for (int n=0;n<lista.darNumeroElementos();n++){

				String[] generos=lista.darElemento(n).getPelicula().getGeneros();
				if(!esta(lista.darElemento(n).getHoraInicio().getTime().getHours(),auxiliar) &&  !estaP(lista.darElemento(n).getPelicula().getTitulo(), auxiliar2)){
					for (int k=0;k<generos.length;k++){

						if(generos[k].equals(genero) ){

							System.out.println("T�tulo de la pel�cula: "+lista.darElemento(n).getPelicula().getTitulo());
							System.out.println("Teatro: "+lista.darElemento(n).getTeatro().getNombre());
							System.out.println("Hora: "+lista.darElemento(n).getHoraInicio().getTime().getHours()+":00");
							auxiliar.agregarAlInicio(lista.darElemento(n).getHoraInicio().getTime().getHours());
							auxiliar2.agregarAlInicio(lista.darElemento(n).getPelicula().getTitulo());
							auxiliar3.agregarAlInicio(nombreTeatro);
						}

					}
				}
			}
		}
		System.out.println("Para su comodidad, y de acuerdo con sus preferencias; le recomendamos ver las pel�culas as�:");
		for(int i=0;i<auxiliar.darNumeroElementos();i++){
			if(sePuedeRecomendar(auxiliar2.darElemento(i)))
			System.out.println(auxiliar2.darElemento(i)+auxiliar.darElemento(i)+":00"+" en "+auxiliar3.darElemento(i));
		}
	}
	
	public static int contarPeliculasGenero(String genero, String teatro, String franja, int dia){
		int contador =0;
		
		if(franja==ManejadorGrafos.FRANJA_MA�ANA){
		
		HashTable<String,ListaEncadenada<VOPeliculaPlan>> tabla=m.getManejadorCarga().darTablaFunciones();
		
		
		ListaEncadenada<VOPeliculaPlan>lista=tabla.darValor(teatro);
		if(lista==null){
			return 0;
		}
		for(int i=0;i<lista.darNumeroElementos();i++){
			String[] generos=lista.darElemento(i).getPelicula().getGeneros();
			for (int j=0;j<generos.length;j++){
				if(generos[j].equals(genero)){
					VOPeliculaPlan pp=lista.darElemento(i);
					 
					contador++;
				}
			
//				if(generos[j].equals(genero)&& lista.darElemento(i).getHoraInicio().HOUR<12 && lista.darElemento(i).getDia()==dia){
//					contador++;
//				}
			}
			
		}
		}
		else if(franja==ManejadorGrafos.FRANJA_TARDE){
			
			HashTable<String,ListaEncadenada<VOPeliculaPlan>> tabla=m.getManejadorCarga().darTablaFunciones();
			ListaEncadenada<VOPeliculaPlan>lista=tabla.darValor(teatro);
			for(int i=0; i<lista.darNumeroElementos();i++){
				if(lista.darElemento(i).getPelicula().getGeneros().equals(genero) && lista.darElemento(i).getHoraInicio().getTime().getHours()>12 && lista.darElemento(i).getHoraInicio().getTime().getHours()<18 && lista.darElemento(i).getDia()==dia){
					contador++;
				}
			}
			}
		else{
			HashTable<String,ListaEncadenada<VOPeliculaPlan>> tabla=m.getManejadorCarga().darTablaFunciones();
			ListaEncadenada<VOPeliculaPlan>lista=tabla.darValor(teatro);
			for(int i=0;i<lista.darNumeroElementos();i++){
				if(lista.darElemento(i).getPelicula().getGeneros().equals(genero) &&  lista.darElemento(i).getHoraInicio().getTime().getHours()>18 && lista.darElemento(i).getDia()==dia){
					contador++;
				}
			}
		}
		return contador;
	}
	public static boolean seAgrego(int idPelicula, ListaEncadenada<VOPelicula> listaPelis){
		for(int i=0;i<listaPelis.darNumeroElementos();i++){
			if(listaPelis.darElemento(i).getId()==idPelicula){
				return true;
			}
		}
		return false;
	}
	public static boolean esta(int hora, ListaEncadenada<Integer> lista){
		for(int i=0;i<lista.darNumeroElementos();i++){
			if(lista.darElemento(i)==hora){
				return true;
			}
		}
		return false;
	}
	public static boolean estaP(String titulo, ListaEncadenada<String> lista){
		for(int i=0;i<lista.darNumeroElementos();i++){
			if(lista.darElemento(i).equals(titulo)){
				return true;
			}
		}
		return false;
	}
	public static boolean sePuedeRecomendar(String nombrePelicula){
		ListaEncadenada<String> listaR=m.getListaRecomendaciones();
		for(int i=0;i<listaR.darNumeroElementos();i++){
			if(listaR.equals(nombrePelicula)){
				return false;
			}
		}
		return true;
		
	}
	
	/*R9
	 * Genera un MST para el grafo de Teatros
	 * @return Lista de aristas pertenecientes al MST encontrado, null si no es posible generar un MST.
	 */
	public 	static void generarMapa() 
	{
		Graph<VOTeatro> grafo=m.getManejadorGrafos().getGrafoMa�ana();
		// TODO Auto-generated method stub
		ListaEncadenada<Edge<VOTeatro>> camino = new ListaEncadenada();
		List<Edge<VOTeatro>> listaEdges=grafo.getEdges();
		List<Vertex<VOTeatro>> listaVertices=grafo.getVerticies();
		Kruskal<VOTeatro> kruskal=new Kruskal<VOTeatro>();
		List<Edge<VOTeatro>> listaRetorno=kruskal.kruskalAlgorithm(listaEdges, listaVertices);
		for(int i=0;i<listaRetorno.size();i++){
			System.out.println(listaRetorno.get(i).getFrom().getData().getNombre());
			System.out.println(listaRetorno.get(i).getTo().getData().getNombre());
		}
		
	}
	
}

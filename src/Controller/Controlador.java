package Controller;

import java.util.Calendar;

import VOS.VOGeneroPelicula;
import VOS.VOPeliculaPlan;
import VOS.VOTeatro;
import data_structures.ListaEncadenada;
import data_structures.Vertex;
import logic.ManejadorPrincipal;
import logic.PRUEBA;

public class Controlador {
private static ManejadorPrincipal manejadorPrincipal=new ManejadorPrincipal();
private static PRUEBA claseDeJorge=new PRUEBA();
/** Requerimiento 1
 * Cargar teatros
 */
public static void cargarTeatros(){
	manejadorPrincipal.cargarTeatros();
}
/** Requerimiento 2
 *  Cargar cartelera
 */
public static void cargarCartelera(){
	manejadorPrincipal.cargarCartelera();
}
/** Requerimiento 3
 * Cargar red
 */
public static void cargarRed(){
	manejadorPrincipal.cargarRed();
}
/** Requerimiento 4
 *  Plan de pel�culas
 */
public static void planPeliculas(Calendar fechaNecesaria, int idUsuario){
	claseDeJorge.planPorFecha(fechaNecesaria, idUsuario);
}
/** Requerimiento 5
 * D�a con m�s pel�culas con g�nero dado
 */
public static void diaMasPeliculas(String genero){
	claseDeJorge.diaMayorCantidadPeliculas(genero);
	
}
/** Requerimiento 6
 *  Plan por franquicia
 */
public static void planPorFranquicia(String franquicia, int fecha, String franja){
	ListaEncadenada<VOPeliculaPlan> lista=manejadorPrincipal.PlanPorFranquicia(franquicia, fecha, franja);
	for(int i=0;i<lista.darNumeroElementos();i++){
		System.out.println("Teatro:"+ lista.darElemento(i).getTeatro()+" Pel�cula: "+lista.darElemento(i).getPelicula().getTitulo());
	}
}
/** Requerimiento 7
 * Calcular agenda
 */
public static void calcularAgenda(String genero, int dia){
	
	manejadorPrincipal.planPorGeneroYDesplazamiento(genero, dia);
}
/** Requerimiento 8
 * Plan por genero, desplazamiento y franquicia
 */
public static void planPorGeneroDesplazamientoYFranquicia(String genero,int fecha, String franquicia){
	ListaEncadenada<VOPeliculaPlan> lista=manejadorPrincipal.planPorGeneroDesplazamientoYFranquicia(genero, fecha, franquicia);
	for(int i=0;i<lista.darNumeroElementos();i++){
		System.out.println("Teatro: "+lista.darElemento(i).getTeatro()+" Pel�cula: "+lista.darElemento(i).getPelicula().getTitulo()+" Horario: "+lista.darElemento(i).getHoraInicio());
	}
	}
/**
 * Requerimiento 9
 * MST
 */
public static void darMapaMST(){
	claseDeJorge.generarMapa();
}
/**
 * Requerimiento 10
 */
public static void rutasPosibles(String nombre, int n){
	VOTeatro aUsar=new VOTeatro();
	aUsar.setNombre(nombre);
	ListaEncadenada<ListaEncadenada<Vertex<VOTeatro>>> lista=manejadorPrincipal.rutasPosible(aUsar, n);
	for(int i=0;i<lista.darNumeroElementos();i++){
		
		for(int j=0;j<lista.darElemento(i).darNumeroElementos();j++){
			System.out.println(lista.darElemento(i).darElemento(j).getData().getNombre());
		}
	}
}



 
}

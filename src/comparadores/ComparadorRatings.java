package comparadores;

import java.util.Comparator;

import VOS.VORating;

/**
 * Clase comparadora de ratings
 */
public class ComparadorRatings implements Comparator<VORating>
{

	/**
	 * Compara dos ratings por valor del rating
	 */
	@Override
	public int compare(VORating r1, VORating r2) 
	{
		if(r1.getRating() > r2.getRating())
		{
			return 1;
		}
		else if(r1.getRating() < r2.getRating())
		{
			return -1;
		}
		else
		{
			return -1;
		}
	}

}

package comparadores;

import java.util.Comparator;

import VOS.VOPeliculaPlan;

public class ComparadorHorasFunciones implements Comparator<VOPeliculaPlan> {

	@Override
	public int compare(VOPeliculaPlan o1, VOPeliculaPlan o2) 
	{
		if(o1.getHoraInicio().compareTo(o2.getHoraInicio()) < 0)
		{
			return -1;
		}
		else if(o1.getHoraInicio().compareTo(o2.getHoraInicio()) > 0)
		{
			return 1;
		}
		else
		{
			return 1;
		}
	}

}

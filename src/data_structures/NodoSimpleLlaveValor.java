package data_structures;

public class NodoSimpleLlaveValor<K extends Comparable<K>,V> 
{
	/**
	 * Atributo que modela el elemento T del nodo
	 */
	private K llave ;
	
	private V valor;

	/**
	 * Atributo que modela el siguiente nodo
	 */
	private NodoSimpleLlaveValor<K,V> siguiente;

	/**
	 * Retorna el siguiente nodo
	 * @return
	 */
	public NodoSimpleLlaveValor<K,V> darSiguiente()
	{
		return siguiente;
	}

	/**
	 * Cambia el siguiente
	 * @param i, nodo cambiar por el siguiente
	 */
	public void cambiarSiguiente(NodoSimpleLlaveValor<K,V> i)
	{
		i.siguiente = siguiente;
		siguiente = i;
	}
	

	/**
	 * Contructore de la clase, se crea el nodo con el elemento T dado por parámetro
	 * @param t del nodo a crear
	 */
	public NodoSimpleLlaveValor(K key, V val) 
	{
		llave=key;
		valor=val;
		siguiente = null;
	}
	
	public void setValor(V val)
	{
		valor=val;
	}
	
	public K darLlave()
	{
		return llave;
	}
	
	public V darValor()
	{
		return valor;
	}
	
	public void setNext(NodoSimpleLlaveValor<K,V> n)
	{
		this.siguiente = n;
	}

	/**
	 * Desconecta el siguiente nodo
	 */
	public void desconectarSiguiente()
	{
		siguiente = siguiente.siguiente; 
	}
}
package data_structures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

import API.ILista;
import API.IRedBlackBST;



/**
 * Clase de arbol rojo-negro adaptada del libro Algorithms (Sedgewick-Wayne) 4th ed. 
 * Implementacion adaptada de: http://algs4.cs.princeton.edu/33balanced/RedBlackBST.java.html
 * @param <K> Clase de las llaves
 * @param <V> Clase de los valores
 */
public class RedBlackBST <K extends Comparable<K>, V> implements IRedBlackBST<K, V> {

	private static final boolean RED = true;
	private static final boolean BLACK = false;

	private Node root;
	private int altura;

	private class Node
	{
		K key;
		V val;
		Node left, right; // Sub�rboles
		int size; // # nodos en este arbol
		boolean color; // color del enlace con el padre

		//Constructor
		Node(K K, V val, int N, boolean color)
		{
			this.key = K;
			this.val = val;
			this.size = N;
			this.color = color;
		}

		//M�todo toString para graficar llave valor por cada nodo
		public String toString()
		{
			return "[" + this.key.toString() + ", " + this.val.toString() + "] ";
		}
	}

	@Override
	public boolean isEmpty() 
	{
		return root == null;
	}

	public void put(K K, V val)
	{ // Buscar la llave, actualiza el valor si la encuentra
		root = put(root, K, val);
		root.color = BLACK;
	}

	//Tama�o de todo el arbol
	public int size()
	{
		return size(root);
	}

	public V get(K key) throws NoSuchElementException
	{
		V value = get(root, key);
		if (key == null && value == null)
			throw new NoSuchElementException("Element not found: [" + key + ", " + value +"]");
		return value; 
	}

	@Override
	public boolean contains(K key) {
		try
		{
			return get(key) != null;
		} 
		catch (Exception e)
		{
			return false;
		}
	}

	public int height() 
	{
		return height(root);
	}

	@Override
	public K min() 
	{

		Node hoja = root;

		if (root != null)
		{
			while (hoja.left != null)
			{
				hoja = hoja.left;
			}
		}
		return hoja.key;
	}

	@Override
	public K max()
	{
		Node hoja = root;

		if (root != null)
			while (hoja.right != null)
				hoja = hoja.right;			

		return hoja.key;
	}


	public void delete(K key) throws NoSuchElementException
	{ 
		//La llave no puede ser null
		if (key == null) 
			throw new NoSuchElementException("Can't delete null Key");
		//Si no est�, no puedo eliminarla
		if (!contains(key)) 
			throw new NoSuchElementException("Element does not exist in the Tree");

		if (!isRed(root.left) && !isRed(root.right))
			root.color = RED;

		root = delete(root, key);

		if (!isEmpty())
			root.color = BLACK;
	}

	public void deleteMin()
	{
		if (!isRed(root.left) && !isRed(root.right))
			root.color = RED;

		root = deleteMin(root);
		if (!isEmpty()) 
			root.color = BLACK;
	}

	public void deleteMax() 
	{
		if (!isRed(root.left) && !isRed(root.right))
			root.color = RED;

		root = deleteMax(root);
		if (!isEmpty()) root.color = BLACK;
	}

	@Override
	public Iterator<K> keys() 
	{

		ArrayList<K> llaves = new ArrayList<K>();

		keys(llaves, root);

		return llaves.iterator();
	}

	@Override
	public boolean check() 
	{
		//Comprobar balance negro (todos los nodos hojas est�n a la misma distancia negra de la ra�z)
		boolean blackBalance = checkBlackBalance();
		//Comprobar el tama�o (el tama�o debe ser menor o igual a 2*Log(n)
		boolean sizeOfTree = height() < 2*(Math.log10(size()) / Math.log10(2.));
		//Ver si es un arbol 2-3
		boolean es23 = is23();
		
		if (!blackBalance)
		{
			System.out.println("Tree not black-balanced: " + darListaDistanciaEnlacesNegros());
		}
		if (!sizeOfTree)
			System.out.println("Tree size not correct");
		if (!es23)
			System.out.println("Tree is not 2-3");

		return blackBalance && sizeOfTree && es23;
	}

	/**
	 * M�todo que "grafica" el �rbol en consola, para darse una peque�a idea de c�mo
	 * est� quedando el �rbol. No es la mejor representaci�n, pero puede ser �til
	 */
	public void graficarArbol()
	{
		graficarArbol(root);
	}

	public String toString ()
	{
		return recorrerArbol(root,0);
	}

	public ArrayList<Integer> darListaDistanciaEnlacesNegros()
	{
		return darListaDistanciaEnlacesNegros(root, null, 0);
	}

    public ILista<K> darLlavesIntervalo(K lo, K hi) {
        if (lo == null) throw new IllegalArgumentException("first argument to Ks() is null");
        if (hi == null) throw new IllegalArgumentException("second argument to Ks() is null");

        ILista<K> lista = new ListaEncadenada<K>();
        //Si el arbol est� vac�o o lo que me entra es un intervalo vac�o, devuelvo una lista vac�a
        //Intervalo vac�o: [1, -1] (donde el primero es menor que el segundo)
        if (isEmpty() || lo.compareTo(hi) > 0)
        	return lista;
        llavesIntervalo(root, lista, lo, hi);
        return lista;
    } 

    // a�adir las llaves dentro del intervalo a la cola, del arbol x
    private void llavesIntervalo(Node x, ILista<K> lista, K lo, K hi) { 
        if (x == null) 
        	return; 
        int cmplo = lo.compareTo(x.key); 
        int cmphi = hi.compareTo(x.key);
        
        if (cmplo < 0) 
        	llavesIntervalo(x.left, lista, lo, hi); 
        if (cmplo <= 0 && cmphi >= 0)
        	lista.agregarElementoFinal(x.key); 
        if (cmphi > 0)
        	llavesIntervalo(x.right, lista, lo, hi); 
    } 
	
	private int height(Node x) 
	{
		if (x == null) 
			return -1;
		//Retorno el m�ximo de mis hijos
		return 1 + Math.max(height(x.left), height(x.right));
	}

	private boolean isRed(Node x)
	{
		if (x == null) return false;
		return x.color == RED;
	}

	private Node put(Node h, K K, V val)
	{
		if (h == null) // Inserci�n est�ndar, con enlace rojo al padre
			return new Node(K, val, 1, RED);
		int cmp = K.compareTo(h.key);
		if (cmp < 0)
			h.left = put(h.left, K, val);
		else if (cmp > 0) 
			h.right = put(h.right, K, val);//Reemplazar llave
		else h.val = val;

		//Casos para mantener el invariante del arbol
		if (isRed(h.right) && !isRed(h.left))
			h = rotateLeft(h);
		if (isRed(h.left) && isRed(h.left.left)) 
			h = rotateRight(h);
		if (isRed(h.left) && isRed(h.right))
			flipColors(h);

		h.size = size(h.left) + size(h.right) + 1;
		return h;
	}

	//Tama�o del subarbol del nodo x
	private int size(Node x)
	{ 
		if (x == null)
			return 0;
		else
			return x.size;
	}

	private Node min(Node x) { 
		if (x.left == null) 
			return x; 
		else
			return min(x.left); 
	}

	private V get(Node x, K key)
	{
		if (x == null)
			return null;
		int cmp = key.compareTo(x.key);
		if (cmp < 0)
			return get(x.left, key);
		else if (cmp > 0) 
			return get(x.right, key);
		else 
			return x.val;
	}

	private String darColor(boolean color)
	{
		if (color == RED)
			return "Rojo";
		else
			return "Negro";
	}

	private String recorrerArbol(Node x, int nivel)
	{

		if (nivel > altura)
			altura = nivel;

		String respuesta = nivel+ " " + darColor(x.color) +"[" + x.key.toString() + "," + x.val.toString() + "]-";

		if (x.left != null)
			respuesta += recorrerArbol(x.left, nivel+1);
		if (x.right != null)
			respuesta += recorrerArbol(x.right, nivel+1);

		return respuesta;
	}

	private void graficarArbol(Node raiz)
	{
		String input = this.toString();
		String[] nodos = input.split("-");
		// Uso altura +1. porque se comienza a contar desde cero (y necesito un espacio m�s)
		String[][] nodosPorNivel = new String[altura+1][];

		for (int i = 0; i < nodosPorNivel.length; i++)
		{//Voy a ir por todos los niveles del arbol
			ArrayList<String> nodosNivelActual = new ArrayList<String>();
			for (int j = 0; j < nodos.length; j++)
			{
				Character nivel = nodos[j].charAt(0);
				int nivelNodo =  Integer.parseInt(nivel.toString());
				if (nivelNodo == i)
					nodosNivelActual.add(nodos[j]);				
			}
			nodosPorNivel[i] = toArray(nodosNivelActual);
		}

		System.out.println("--------------------------------------");

		for (int i = 0; i< nodosPorNivel.length; i++)
		{
			String lineaNivel = "";
			for (int j = 0; j< nodosPorNivel[i].length; j++)
			{
				lineaNivel += nodosPorNivel[i][j] + generarEspacios(size(root)*altura/(i+1));
			}
			System.out.println(generarEspacios(10*altura/(i+1)-5) + lineaNivel + "\n");
		}
		System.out.println("--------------------------------------");
		System.out.println("�rbol como arreglo por niveles: [0,1...n]");
		System.out.println(Arrays.deepToString(nodosPorNivel));
	}

	private String generarEspacios(int cantidadDeEspacios) {

		String respuesta = "";

		for (int i = 0; i < cantidadDeEspacios; i++)
			respuesta+= " ";

		return respuesta;
	}

	private String[] toArray(ArrayList<String> lista)
	{
		String[] respuesta = new String[lista.size()];

		for (int i = 0; i < lista.size(); i++)
			respuesta[i] = lista.get(i);		

		return respuesta;
	}

	private ArrayList<Integer> darListaDistanciaEnlacesNegros(Node x, ArrayList<Integer> lista, int contadorEnlacesNegros)
	{
		if (lista == null)
			lista = new ArrayList<Integer>();

		if (x != null)
		{
			if (x.color == BLACK)
				contadorEnlacesNegros++;
			if (x.right == null)
				lista.add(contadorEnlacesNegros);
			else
				darListaDistanciaEnlacesNegros(x.right, lista, contadorEnlacesNegros);
			if (x.left == null)
				lista.add(contadorEnlacesNegros);
			else
				darListaDistanciaEnlacesNegros(x.left, lista, contadorEnlacesNegros);		
		}

		return lista;
	}

	//Rotaci�n a la derecha
	private Node rotateRight(Node h)
	{
		Node x = h.left;
		h.left = x.right;
		x.right = h;
		x.color = h.color;
		h.color = RED;
		x.size = h.size;
		h.size = 1 + size(h.left)
				+ size(h.right);
		return x;
	}

	//Rotaci�n a la izquierda
	private Node rotateLeft(Node h)
	{
		Node x = h.right;
		h.right = x.left;
		x.left = h;
		x.color = h.color;
		h.color = RED;
		x.size = h.size;
		h.size = 1 + size(h.left)
				+ size(h.right);
		return x;
	}

	private void flipColors(Node h)
	{
		h.color = RED;
		h.left.color = BLACK;
		h.right.color = BLACK;
	}
	private Node deleteMin(Node h) 
	{ 
		if (h.left == null)
			return null;

		if (!isRed(h.left) && !isRed(h.left.left))
			h = moveRedLeft(h);

		h.left = deleteMin(h.left);
		return balance(h);
	}

	private Node deleteMax(Node h) 
	{ 
		if (isRed(h.left))
			h = rotateRight(h);

		if (h.right == null)
			return null;

		if (!isRed(h.right) && !isRed(h.right.left))
			h = moveRedRight(h);

		h.right = deleteMax(h.right);

		return balance(h);
	}

	// Eliminar
	private Node delete(Node h, K key) 
	{ 
		if (key.compareTo(h.key) < 0)  {
			if (!isRed(h.left) && !isRed(h.left.left))
				h = moveRedLeft(h);
			h.left = delete(h.left, key);
		}
		else {
			if (isRed(h.left))
				h = rotateRight(h);
			if (key.compareTo(h.key) == 0 && (h.right == null))
				return null;
			if (!isRed(h.right) && !isRed(h.right.left))
				h = moveRedRight(h);
			if (key.compareTo(h.key) == 0) {
				Node x = min(h.right);
				h.key = x.key;
				h.val = x.val;
				h.val = get(h.right, min(h.right).key);//
				h.key = min(h.right).key;//
				h.right = deleteMin(h.right);
			}
			else h.right = delete(h.right, key);
		}
		return balance(h);
	}

	//Recibo una lista, me agrego y hago recursivamente con mis hijos hasta llegar a una hoja (x = null)
	private void keys (ArrayList<K> lista, Node x)
	{
		if (x != null)
		{
			lista.add(x.key);
			keys(lista, x.right);
			keys(lista, x.left);
		}
	}

	private Node moveRedLeft(Node h) {
		// assert (h != null);
		// assert isRed(h) && !isRed(h.left) && !isRed(h.left.left);

		flipColors(h);
		if (isRed(h.right.left)) { 
			h.right = rotateRight(h.right);
			h = rotateLeft(h);
			flipColors(h);
		}
		return h;
	}

	private Node moveRedRight(Node h) {
		flipColors(h);
		if (isRed(h.left.left)) { 
			h = rotateRight(h);
			flipColors(h);
		}
		return h;
	}

	private Node balance(Node h) 
	{
		if (isRed(h.right))  
			h = rotateLeft(h);
		if (isRed(h.left) && isRed(h.left.left))
			h = rotateRight(h);
		if (isRed(h.left) && isRed(h.right)) 
			flipColors(h);

		h.size = size(h.left) + size(h.right) + 1;
		return h;
	}

	private boolean checkBlackBalance()
	{
		ArrayList<Integer> alturaNegraDeHojas = darListaDistanciaEnlacesNegros();

		//Como la altura debe ser igual para todas las hojas, basta con cojer cualquiera y ver si cambia
		int constante = alturaNegraDeHojas.get(0);
		for (Integer altura:alturaNegraDeHojas)
			if (altura != constante)
				return false;
		return true;
	}

	private boolean is23() 
	{
		return is23(root); 
	}


	// Revisar si es 2-3 a partir del color de todos los nodos (que por mucho, solo el hijo izquierdo sea rojo)
	// Revisar que todos los rojos estpen a la izquierda
	private boolean is23(Node x) 
	{
		if (x == null) 
			return true;
		if (isRed(x.right))
			return false;
		if (x != root && isRed(x) && isRed(x.left))
			return false;
		return is23(x.left) && is23(x.right);
	} 
	
//	public static void main(String[] args) {
//
//		RedBlackBST<String, Integer> arbol = new RedBlackBST<String, Integer>();
//
//		arbol.put("A", 1);
//		arbol.put("B", 2);
//		arbol.put("C", 3);
//		arbol.put("D", 4);
//		arbol.put("E", 5);
//		arbol.put("F", 6);
//		arbol.put("G", 7);
//		arbol.put("H", 8);
//		arbol.put("I", 9);
//		arbol.put("J", 10);
//		arbol.put("Z", 30);
//
//		System.out.println(arbol.toString());
//		System.out.println(arbol.size(arbol.root));
//		System.out.println("Altura del arbol: " + arbol.altura);
//		arbol.graficarArbol(arbol.root);
//
//		System.out.println(arbol.darListaDistanciaEnlacesNegros(arbol.root, null, 0));
//		//P�gina cool (y r�pida) para graficar: http://bst.jtcho.me/llrb
//	}
	
	public ListaEncadenada<V> treeToList()
	{
		ListaEncadenada<V> list = new ListaEncadenada<>();
		Iterator<K> iter = keys();
		while(iter.hasNext())
		{
			K current = iter.next();
			list.agregarAlInicio(get(current));
		}
		return list;
	}
}


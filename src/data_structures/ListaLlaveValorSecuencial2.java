package data_structures;

import java.util.Iterator;

import API.IListaLlaveValorSecuencial;



public class ListaLlaveValorSecuencial2 <K,V> implements IListaLlaveValorSecuencial<K, V>, Iterable<NodoLlaveValor<K, V>>{

	private ListaEncadenada<NodoLlaveValor<K, V>> nodos;

	private int tamanio;

	@Override
	public int darTamanio() {
		return tamanio;
	}

	@Override
	public boolean estaVacia() {
		return tamanio == 0;
	}

	public ListaLlaveValorSecuencial2()
	{
		nodos =  new ListaEncadenada<NodoLlaveValor<K, V>>();
		tamanio = 0;
	}

	@Override
	public boolean tieneLlave(K llave) {

		boolean tiene = false;

		Iterator<NodoLlaveValor<K, V>> iterNodos = nodos.iterator();

		while (iterNodos.hasNext() && !tiene)
		{
			tiene = iterNodos.next().getLlave().equals(llave);
		}

		return tiene;
	}

	@Override
	public V darValor(K llave) {

		V buscado = null;

		Iterator<NodoLlaveValor<K, V>> iterNodos = nodos.iterator();

		while (iterNodos.hasNext() && buscado == null)
		{
			NodoLlaveValor<K, V> actual = iterNodos.next();
			buscado = actual.consultar(llave);
		}

		return buscado;
	}

	@Override
	public void insertar(K llave, V valor) {

		for(NodoLlaveValor<K, V> nodoActual:nodos)
		{
			if(nodoActual.getLlave().equals(llave))
			{
				if(valor == null)
				{
					nodos.eliminarNodo(nodoActual);
				}
				else
				{
					nodoActual.cambiarNodo(llave, valor);
				}
				return;
			}
		}
		nodos.agregarAlInicio(new NodoLlaveValor<K,V>(llave, valor));
	}

	@Override
	public Iterator<K> llaves() {
		return new IteradorLlaves(this);
	}


	public String toString()
	{
		String respuesta = "";

		Iterator<K> llaves = llaves();

		while (llaves.hasNext())
		{
			respuesta += llaves.next() +", ";
		}

		return respuesta;
	}

	@Override
	public Iterator<NodoLlaveValor<K, V>> iterator() {
		return nodos.iterator();
	}

	private class IteradorLlaves implements Iterator<K>
	{
		private ListaLlaveValorSecuencial2<K, V> listaLlaveValor;

		private Iterator<NodoLlaveValor<K, V>> iteradorNodos;

		private IteradorLlaves(ListaLlaveValorSecuencial2<K, V> listaLlaveValor)
		{
			this.listaLlaveValor = listaLlaveValor;
			iteradorNodos = listaLlaveValor.iterator();
		}


		@Override
		public boolean hasNext() {
			return iteradorNodos.hasNext();
		}

		@Override
		public K next() {
			return iteradorNodos.next().getLlave();
		}


		@Override
		public void remove() {
			// TODO Auto-generated method stub
			
		}

	}
}

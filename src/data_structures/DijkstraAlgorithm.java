package data_structures;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class DijkstraAlgorithm<T> {

    private final List<Vertex<T>> nodes;
    private final List<Edge<T>> edges;
    private Set<Vertex<T>> settledNodes;
    private Set<Vertex<T>> unSettledNodes;
    private Map<Vertex<T>, Vertex<T>> predecessors;
    private Map<Vertex<T>, Integer> distance;

    public DijkstraAlgorithm(Graph graph) {
        // create a copy of the array so that we can operate on this array
        this.nodes = new ArrayList<Vertex<T>>(graph.getVerticies());
        this.edges = new ArrayList<Edge<T>>(graph.getEdges());
    }

    public void execute(Vertex<T> source) {
        settledNodes = new HashSet<Vertex<T>>();
        unSettledNodes = new HashSet<Vertex<T>>();
        distance = new HashMap<Vertex<T>, Integer>();
        predecessors = new HashMap<Vertex<T>, Vertex<T>>();
        distance.put(source, 0);
        unSettledNodes.add(source);
        while (unSettledNodes.size() > 0) {
            Vertex<T> node = getMinimum(unSettledNodes);
            settledNodes.add(node);
            unSettledNodes.remove(node);
            findMinimalDistances(node);
        }
    }

    private void findMinimalDistances(Vertex<T> node) {
        List<Vertex<T>> adjacentNodes = getNeighbors(node);
        for (Vertex<T> target : adjacentNodes) {
            if (getShortestDistance(target) > getShortestDistance(node)
                    + getDistance(node, target)) {
                distance.put(target, getShortestDistance(node)
                        + getDistance(node, target));
                predecessors.put(target, node);
                unSettledNodes.add(target);
            }
        }

    }

    public int getDistance(Vertex<T> node, Vertex<T> target) {
        for (Edge<T> edge : edges) {
            if (edge.getFrom().equals(node)
                    && edge.getTo().equals(target)) {
                return edge.getCost();
            }
        }
        throw new RuntimeException("Should not happen");
    }

    private List<Vertex<T>> getNeighbors(Vertex<T> node) {
        List<Vertex<T>> neighbors = new ArrayList<Vertex<T>>();
        for (Edge<T> edge : edges) {
            if (edge.getFrom().equals(node)
                    && !isSettled(edge.getTo())) {
                neighbors.add(edge.getTo());
            }
        }
        return neighbors;
    }

    private Vertex<T> getMinimum(Set<Vertex<T>> vertexes) {
        Vertex<T> minimum = null;
        for (Vertex<T> vertex : vertexes) {
            if (minimum == null) {
                minimum = vertex;
            } else {
                if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
                    minimum = vertex;
                }
            }
        }
        return minimum;
    }

    private boolean isSettled(Vertex<T> vertex) {
        return settledNodes.contains(vertex);
    }

    private int getShortestDistance(Vertex<T> destination) {
        Integer d = distance.get(destination);
        if (d == null) {
            return Integer.MAX_VALUE;
        } else {
            return d;
        }
    }

    /*
     * This method returns the path from the source to the selected target and
     * NULL if no path exists
     */
    public LinkedList<Vertex<T>> getPath(Vertex<T> target) {
        LinkedList<Vertex<T>> path = new LinkedList<Vertex<T>>();
        Vertex<T> step = target;
        // check if a path exists
        if (predecessors.get(step) == null) {
            return null;
        }
        path.add(step);
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }
        // Put it into the correct order
        Collections.reverse(path);
        return path;
    }
    

}
package data_structures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Kruskal<T> 
{
	private List<Edge<T>> listOfEdges;
	
	public List<Edge<T>> kruskalAlgorithm(List<Edge<T>> edges, List<Vertex<T>> verticies)
	{
		listOfEdges = new ArrayList<Edge<T>>();
		DisjointSet ds = new DisjointSet(verticies.size());
		Collections.sort(edges);
		for(int i=0; i != edges.size() && listOfEdges.size() != verticies.size()-1; i++)
		{
			Edge<T> e = edges.get(i);
			if(ds.find(e.getFrom().getNum()) != ds.find(e.getTo().getNum()))
			{
				listOfEdges.add(e);
			}
		}
		return listOfEdges;
	}
}

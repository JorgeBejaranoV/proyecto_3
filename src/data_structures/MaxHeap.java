package data_structures;

import java.lang.reflect.Array;
import java.util.Arrays;

import API.IColaPrioridad;


public class MaxHeap <T extends Comparable<T>> implements IColaPrioridad<T>
{
	private int tamanoMax;

	private T[] maxHeap;

	private int numElementos;

	private Class<T> claseEstructura;


	public MaxHeap(Class<T> claseEntrada)
	{
		numElementos = 0;
		claseEstructura = claseEntrada;
	}


	@SuppressWarnings("unchecked")
	public void crearCP(int max) 
	{
		maxHeap = (T[]) Array.newInstance(claseEstructura, max + 1);
		tamanoMax = max;
	}

	@Override
	public int darNumElementos()
	{
		return numElementos;
	}



	@Override
	public void agregar(T elemento) throws Exception
	{
		if(numElementos < tamanoMax)
		{
			maxHeap[++numElementos] = elemento;
			swim(numElementos);
		}
		else
		{
			throw new Exception("No cabe nada m�s en el Heap, please borre algo");
		}
	}



	@Override
	public T max() {
		T max = maxHeap[1]; // Cojo la primera
		exch(1, numElementos--); // La cambio con el �ltimo
		maxHeap[numElementos+1] = null; //Elimino el elemento (como ya lo guarde, no hya l�o)
		sink(1); //Ordeno el Heap resultante
		return max;
	}



	@Override
	public boolean esVacia() 
	{
		return numElementos == 0;
	}



	@Override
	public int tamanoMax() 
	{
		return tamanoMax;
	}

	private boolean less(int i, int j)
	{ 
		return maxHeap[i].compareTo(maxHeap[j]) < 0; 
	}

	private void exch(int i, int j)
	{
		T temp = maxHeap[i]; 
		maxHeap[i] = maxHeap[j];
		maxHeap[j] = temp; 
	}

	private void swim(int k)
	{	//si no es la ra�z y el padre es menor que el hijo
		while (k > 1 && less(k/2, k))
		{
			exch(k/2, k); //Cambio el padre por el hijo
			k = k/2;
		}
	}


	private void sink(int k)
	{
		//Que sus hijos existan dentro del arreglo
		while (2*k <= numElementos)
		{
			int j = 2*k;
			//Que exista dentro del arreglo y que j sea menor a su "hermano"
			if ( j < numElementos && less(j, j+1) )
				j++; //Cojo el mayor (su hermano)
			if (!less(k, j)) //Si k no es menor que j
				break;//Entonces est�n ordenados, y ya debo parar
			exch(k, j); // Si s�, entonces los cambio y contin�o
			k = j;
		}
	}
	public T[] darArreglo()
	{
		return maxHeap;
	}

	public String toString ()
	{
		return Arrays.toString(maxHeap);
	}
}

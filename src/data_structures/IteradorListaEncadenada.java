package data_structures;

import java.util.Iterator;


public class IteradorListaEncadenada<T> implements Iterator<T>
{

	private NodoEncadenado<T> actual;

	public IteradorListaEncadenada(ListaEncadenada<T> listaEncadenada)
	{
		this(listaEncadenada.darPrimero());
	}
	
	private IteradorListaEncadenada(NodoEncadenado<T> nodo)
	{
		actual = nodo;
	}
	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return actual != null;
	}

	@Override
	public T next() {
		// TODO Auto-generated method stub
		NodoEncadenado<T> nodoARetornar = actual;
		actual = actual.darSiguiente();
		return nodoARetornar.darElemento();
	}
	public IteradorListaEncadenada<T> darIteradorDesdeSiguiente()
	{
		return new IteradorListaEncadenada<T>(actual.darSiguiente());
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}

}

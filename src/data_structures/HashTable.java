package data_structures;

import java.util.Arrays;
import java.util.Iterator;

import API.IEncadenamientoSeparadoTH;
import API.ILista;



public class HashTable<K, V> implements IEncadenamientoSeparadoTH<K, V>, Iterable<K>{

	private int tamanio;

	private ListaLlaveValorSecuencial2<K, V>[] lista;

	private ListaEncadenada<K> listaDeLlaves;

	public HashTable (int m)
	{
		this.tamanio = 0;

		this.listaDeLlaves = new ListaEncadenada<K>();

		this.lista = new ListaLlaveValorSecuencial2[m];
	}


	@Override
	public int darTamanio() {
		return tamanio;
	}

	public double darFactorDeCarga()
	{
		return (tamanio/lista.length);
	}

	@Override
	public boolean estaVacia() {
		return tamanio == 0;
	}

	@Override
	public boolean tieneLlave(K llave) {

		int codigo = hash(llave);
		if(lista[codigo] == null)
		{
			return false;
		}
		return lista[codigo].tieneLlave(llave);
	}

	@Override
	public V darValor(K llave) {
		int codigo = hash(llave);
//		System.out.println(codigo);
		if(lista[codigo] == null)
		{
			return null;
		}
		return lista[codigo].darValor(llave);
	}

	private int hash(K llave)
	{
		return (llave.hashCode() & 0x7fffffff) % lista.length;	
	}

	@Override
	public void insertar(K llave, V valor) {
		insertarConOSinRehash(llave, valor, true);
	}
	
	private void insertarConOSinRehash(K llave, V valor, boolean hacerRehash) {
		int posicion = hash(llave);
		ListaLlaveValorSecuencial2<K, V> listaPosicion = lista[posicion];
		if(listaPosicion == null)
		{
			listaPosicion = new ListaLlaveValorSecuencial2<>();
			lista[posicion] = listaPosicion;
		}
		boolean tieneLlave = listaPosicion.tieneLlave(llave);

		if(tieneLlave)
		{
			if(valor == null)
			{
				listaDeLlaves.eliminarNodo(llave);
				tamanio --;
				listaPosicion.insertar(llave, valor);
				if(hacerRehash)
				{
					rehashTableEliminar();
				}
			}
			else
			{
				listaPosicion.insertar(llave, valor);				
			}
		}
		else
		{
			if(valor != null)
			{
				
				listaDeLlaves.agregarAlInicio(llave);
				listaPosicion.insertar(llave, valor);
				tamanio ++;
				if(hacerRehash)
				{
					rehashTableAgregar();
				}
			}
		}
	}

	public void rehashTableAgregar ()
	{

		double factorDeCarga = tamanio/lista.length;
		if (factorDeCarga >= 8)
		{
			//M�s grande
			resize(2*lista.length);

		}
	}

	public void rehashTableEliminar ()
	{

		double factorDeCarga = tamanio/lista.length;

		if (factorDeCarga <= 2 && lista.length > 2)
		{
			//Miti miti
			resize(lista.length/2);
		}
	}

	private void resize(int nuevoTamanio) {

		if(nuevoTamanio != 0)
		{
			HashTable<K, V> nuevaTH = new HashTable<K, V>(nuevoTamanio);

			for(ListaLlaveValorSecuencial2<K, V> listaActual:lista)
			{
				if(listaActual != null)
				{
					for(NodoLlaveValor<K, V> nodoActual:listaActual)
					{
						K llaveActual = nodoActual.getLlave();
						V valorActual = nodoActual.getValor();
						nuevaTH.insertarConOSinRehash(llaveActual, valorActual, false);
					}
				}
			}

			this.lista = nuevaTH.lista;	
		}
	}

	@Override
	public Iterator<K> llaves() {
		return iterator();
//		ListaEncadenada<K> listaDeLlavesTemp =  new ListaEncadenada<K>();
//
//		for (ListaLlaveValorSecuencial2<K,V> listasColisiones : lista)
//		{
//			Iterator<K> listaLocal = listasColisiones.llaves();
//
//			while (listaLocal.hasNext())
//			{
//				listaDeLlavesTemp.agregarElementoFinal(listaLocal.next());
//			}
//		}
//
//		final ListaEncadenada<K> listaDeLlaves = listaDeLlavesTemp;
//
//		Iterator<K> iterLista = new Iterator<K> () {
//
//			Iterator<K> iterLlaves = listaDeLlaves.iterator();
//
//			@Override
//			public boolean hasNext() {
//				return iterLlaves.hasNext();
//			}
//
//			@Override
//			public K next() {
//				return iterLlaves.next();
//			}
//		};
//
//		return iterLista;
	}

	@Override
	public int[] darLongitudListas() {

		int[] longitudListas = new int[lista.length];

		for (int i = 0; i< lista.length; i++)
		{
			longitudListas[i] = lista[i].darTamanio();
		}

		return longitudListas;
	}

	public String toString()
	{
		return Arrays.toString(lista);
	}

	@Override
	public Iterator<K> iterator() {
		return listaDeLlaves.iterator();
	}

	public Iterator<V> iteratorValues()
	{
		return new IteradorValores(this);
	}

	private class IteradorValores implements Iterator<V>
	{
		private HashTable<K, V> hashTable;

		private Iterator<K> iteradorLlaves;

		private IteradorValores(HashTable<K, V> hashTable)
		{
			this.hashTable = hashTable;
			iteradorLlaves = hashTable.iterator();
		}


		@Override
		public boolean hasNext() {
			return iteradorLlaves.hasNext();
		}

		@Override
		public V next() {
			return hashTable.darValor(iteradorLlaves.next());
		}


		@Override
		public void remove() {
			// TODO Auto-generated method stub
			
		}

	}

}

package data_structures;

public class NodoEncadenado<T> {

	private NodoEncadenado<T> siguiente;

	private T elemento;

	public NodoEncadenado(T elemento)
	{
		this.elemento = elemento;
		siguiente = null;
	}

	public NodoEncadenado<T> agregarElementoFinal(T elemento) {
		if(siguiente == null)
		{
			siguiente = new NodoEncadenado<T>(elemento);
			return siguiente;
		}
		else
		{
			return siguiente.agregarElementoFinal(elemento);

		}
	}

	public NodoEncadenado<T> darSiguiente()
	{
		return siguiente;
	}
	public T darElemento()
	{
		return elemento;
	}

	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		if(siguiente == null)
		{
			return 1;
		}
		else
		{
			return 1+siguiente.darNumeroElementos();
		}

	}



	public void cambiarSiguiente( NodoEncadenado<T> nodo)
	{
		siguiente = nodo;
	}

	public void cambiarElemento(T elem)
	{
		elemento = elem;
	}
	
	public String toString()
	{
		return "1";
	}
}


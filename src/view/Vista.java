package view;

import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import Controller.Controlador;



public class Vista {


	private static void printMenu(){
		System.out.println("---------------------------------------");
		System.out.println("Bienvenido al Sistema de recomendaci�n");
		System.out.println("---------------------------------------");
		System.out.println("Por favor, seleccione una de las siguientes opciones:");
		System.out.println("1. Cargar teatros");
		System.out.println("2. Cargar cartelera");
		System.out.println("3. Cargar red");
		System.out.println("4. Dar un plan");
		System.out.println("5. Consultar d�a con m�s pel�culas de un g�nero");
		System.out.println("6. Dar plan por franquicia");
		System.out.println("7. Calcular agenda");
		System.out.println("8. Plan por g�nero, desplazamiento y franquicia");
		System.out.println("9. Mejor mapa");
		System.out.println("10. Ruta posible");
		System.out.println("---Por favor escribe la opci�n y presiona 'Enter'---");
	}


	public static void main(String[] args) throws InterruptedException{

	
		
		Scanner sc = new Scanner(System.in);

		for(;;){
			printMenu();


			int option = sc.nextInt();
			
			switch(option){
			case 1: 
				   Controlador.cargarTeatros();
				
			break;
			case 2: 
			Controlador.cargarCartelera();;
			break;
			case 3:
				Controlador.cargarRed();
				break;
			case 4: System.out.println("Por favor ingrese el id del usuario");
			int idUsuario=leerIdUsuario();
			System.out.println("Por favor ingrese la fecha as� YYYY/MM/DD/HH");
			String cadena=leerRuta();
			String[] cadenaFraccionada=cadena.split("/");
			Calendar fecha= Calendar.getInstance();
			fecha.set(Integer.parseInt(cadenaFraccionada[0]), Integer.parseInt(cadenaFraccionada[1]), 1, Integer.parseInt(cadenaFraccionada[2]), Integer.parseInt(cadenaFraccionada[3]));
			Controlador.planPeliculas(fecha, idUsuario);
			break;
			case 5:
				System.out.println("Por favor ingrese el g�nero a consultar");
				String genero=leerRuta();
				Controlador.diaMasPeliculas(genero);
			break;
			case 6:
				System.out.println("Por favor ingrese la franquicia");
				String franquicia= leerRuta();
				System.out.println("Por favor ingrese la fecha");
				int fecha2=leerAno();
				System.out.println("Por favor ingrese la franja");
				String franja=leerRuta();
				Controlador.planPorFranquicia(franquicia, fecha2, franja);
			break;
			case 7:
				System.out.println("Por favor ingrese el g�nero");
				String genero3= leerRuta();
				System.out.println("Por favor ingrese el d�a");
				int dia=leerAno();
				Controlador.calcularAgenda(genero3, dia);
				
				break;
			case 8:
				System.out.println("Por favor ingrese el g�nero");
				String genero2=leerRuta();
				System.out.println("Por favor ingrese la fecha");
				int fecha3=leerAno();
				System.out.println("Por favor ingrese la franquicia");
				String franquicia2=leerRuta();
				Controlador.planPorGeneroDesplazamientoYFranquicia(genero2, fecha3, franquicia2);
				break;
				
			case 9:
				Controlador.darMapaMST();
				break;
			case 10:
				System.out.println("Por fvor ingrese el nombre");
				String nombre=leerRuta();
				System.out.println("Por favor ingrese 'n'");
				int n=leerAno();
				Controlador.rutasPosibles(nombre, n);
			break;
			default: System.out.println("--------- \n Opci�n inv�lida \n---------");
			}
			
			
		}
	}
	
private static int leerOpcion(){
		
		int opcion = 0;
		
		try{

			String line = new Scanner(System.in).nextLine();

			 opcion = Integer.parseInt(line);
			
		}catch(Exception ex){
			System.out.println("Por favor, escriba bien");
		}
		return opcion;
	}

private static int leerIdUsuario(){
	int opcion = 0;
	
	try{

		String line = new Scanner(System.in).nextLine();

		 opcion = Integer.parseInt(line);
		
	}catch(Exception ex){
		System.out.println("�Es muy dif�cil escribir un par de n�meros?");
	}
	return opcion;
}
	private static String leerRuta(){
		String line="";
		try{

			 line = new Scanner(System.in).nextLine();

			
		}catch(Exception ex){
			System.out.println("�Es muy dif�cil escribir un par de n�meros?");
		}
		return line;

	}
	private static int leerAno(){
		int opcion = 0;
		
		try{

			String line = new Scanner(System.in).nextLine();

			 opcion = Integer.parseInt(line);
			
		}catch(Exception ex){
			System.out.println("�Es muy dif�cil escribir un par de n�meros?");
		}
		return opcion-1900;
	}
}

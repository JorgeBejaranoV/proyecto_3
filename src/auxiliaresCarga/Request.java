package auxiliaresCarga;

import com.google.gson.annotations.SerializedName;

public class Request 
{
	@SerializedName("request")
	private DataRequest request;

	public DataRequest getRequest() {
		return request;
	}

	public void setRequest(DataRequest request) {
		this.request = request;
	}
	
	
}

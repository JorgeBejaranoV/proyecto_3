package auxiliaresCarga;

import com.google.gson.annotations.SerializedName;

public class DataRequest 
{
	@SerializedName("user_name")
	private String user_name;
	
	@SerializedName("ratings")
	private DataRatingRequest[] ratings;

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public DataRatingRequest[] getRatings() {
		return ratings;
	}

	public void setRatings(DataRatingRequest[] ratings) {
		this.ratings = ratings;
	}
	
	
}

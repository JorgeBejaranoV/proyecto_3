package auxiliaresCarga;

import com.google.gson.annotations.SerializedName;

public class DataTiempo1 
{
	@SerializedName("Teatro 1")
	private String teatro1;
	
	@SerializedName("Teatro 2")
	private String teatro2;
	
	@SerializedName("Tiempo (minutos)")
	private String tiempo;

	public String getTeatro1() 
	{
		return teatro1;
	}

	public void setTeatro1(String teatro1) 
	{
		this.teatro1 = teatro1;
	}

	public String getTeatro2() {
		return teatro2;
	}

	public void setTeatro2(String teatro2) {
		this.teatro2 = teatro2;
	}

	public String getTiempo() {
		return tiempo;
	}

	public void setTiempo(String tiempo) {
		this.tiempo = tiempo;
	}
	
	
}

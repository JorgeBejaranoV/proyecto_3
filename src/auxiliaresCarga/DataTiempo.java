package auxiliaresCarga;

import com.google.gson.annotations.SerializedName;

public class DataTiempo
{
	@SerializedName("Tiempo")
	private String tiempo;
	
	@SerializedName("Destino")
	private String destino;
	
	@SerializedName("Origen")
	private String origen;
	
	public String getTiempo() 
	{
		return tiempo;
	}
	public void setTiempo(String tiempo) 
	{
		this.tiempo = tiempo;
	}
	public String getDestino()
	{
		return destino;
	}
	public void setDestino(String destino)
	{
		this.destino = destino;
	}
	public String getOrigen() 
	{
		return origen;
	}
	public void setOrigen(String origen)
	{
		this.origen = origen;
	}
	
	
}

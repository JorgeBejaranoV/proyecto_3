package auxiliaresCarga;

import com.google.gson.annotations.SerializedName;

public class DataMoviesFiltered 
{	
	@SerializedName("movie_id")
	private String movieId;
	@SerializedName("title")
	private String title;
	@SerializedName("genres")
	private String genres;
	
	public String getMovieId() 
	{
		return movieId;
	}
	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getGenres() {
		return genres;
	}
	public void setGenres(String genres) {
		this.genres = genres;
	}
	
	
}

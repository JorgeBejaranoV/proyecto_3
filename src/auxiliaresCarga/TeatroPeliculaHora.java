package auxiliaresCarga;

import java.util.Date;

import com.google.gson.annotations.SerializedName;

public class TeatroPeliculaHora 
{
	private String teatro;
	
	public class PeliculaHoras
	{
		@SerializedName("Id")
		private long id;
		@SerializedName("Funciones")
		private Date[] funciones;
		
	}
	
}

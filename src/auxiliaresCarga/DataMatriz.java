

package auxiliaresCarga;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataMatriz 
{
	@SerializedName("493405")
	@Expose
	private String _493405;
	@SerializedName("498381")
	@Expose
	private String _498381;
	@SerializedName("1219827")
	@Expose
	private String _1219827;
	@SerializedName("1293847")
	@Expose
	private String _1293847;
	@SerializedName("1691916")
	@Expose
	private String _1691916;
	@SerializedName("1730768")
	@Expose
	private String _1730768;
	@SerializedName("1753383")
	@Expose
	private String _1753383;
	@SerializedName("2072233")
	@Expose
	private String _2072233;
	@SerializedName("2398241")
	@Expose
	private String _2398241;
	@SerializedName("2568862")
	@Expose
	private String _2568862;
	@SerializedName("2582576")
	@Expose
	private String _2582576;
	@SerializedName("2763304")
	@Expose
	private String _2763304;
	@SerializedName("2771200")
	@Expose
	private String _2771200;
	@SerializedName("3171832")
	@Expose
	private String _3171832;
	@SerializedName("3315342")
	@Expose
	private String _3315342;
	@SerializedName("3401882")
	@Expose
	private String _3401882;
	@SerializedName("3405236")
	@Expose
	private String _3405236;
	@SerializedName("3462710")
	@Expose
	private String _3462710;
	@SerializedName("3717490")
	@Expose
	private String _3717490;
	@SerializedName("3731562")
	@Expose
	private String _3731562;
	@SerializedName("3874544")
	@Expose
	private String _3874544;
	@SerializedName("3896198")
	@Expose
	private String _3896198;
	@SerializedName("3922818")
	@Expose
	private String _3922818;
	@SerializedName("4030600")
	@Expose
	private String _4030600;
	@SerializedName("4116284")
	@Expose
	private String _4116284;
	@SerializedName("4217392")
	@Expose
	private String _4217392;
	@SerializedName("4287320")
	@Expose
	private String _4287320;
	@SerializedName("4425200")
	@Expose
	private String _4425200;
	@SerializedName("4465564")
	@Expose
	private String _4465564;
	@SerializedName("4481414")
	@Expose
	private String _4481414;
	@SerializedName("4581576")
	@Expose
	private String _4581576;
	@SerializedName("4630562")
	@Expose
	private String _4630562;
	@SerializedName("4849438")
	@Expose
	private String _4849438;
	@SerializedName("5052448")
	@Expose
	private String _5052448;
	@SerializedName("5155780")
	@Expose
	private String _5155780;
	@SerializedName("5442430")
	@Expose
	private String _5442430;
	@SerializedName("5460276")
	@Expose
	private String _5460276;
	@SerializedName("5607714")
	@Expose
	private String _5607714;
	@SerializedName("5710514")
	@Expose
	private String _5710514;
	@SerializedName("5982852")
	@Expose
	private String _5982852;
	@SerializedName("movieId")
	@Expose
	private Integer movieId;
	//
	public String get_493405() {
		return _493405;
	}
	//
	public String get_498381() {
		return _498381;
	}
	//
	public String get_1219827() {
		return _1219827;
	}
	//
	public String get_1293847() {
		return _1293847;
	}
	//
	public String get_1691916() {
		return _1691916;
	}
	//
	public String get_1730768() {
		return _1730768;
	}
	//
	public String get_1753383() {
		return _1753383;
	}
	//
	public String get_2072233() {
		return _2072233;
	}
	//
	public String get_2398241() {
		return _2398241;
	}
	//
	public String get_2568862() {
		return _2568862;
	}
	//
	public String get_2582576() {
		return _2582576;
	}
	//
	public String get_2763304() {
		return _2763304;
	}
	//
	public String get_2771200() {
		return _2771200;
	}
	//
	public String get_3171832() {
		return _3171832;
	}
	//
	public String get_3315342() {
		return _3315342;
	}
	//
	public String get_3401882() {
		return _3401882;
	}
	//
	public String get_3405236() {
		return _3405236;
	}
	//
	public String get_3462710() {
		return _3462710;
	}
	//
	public String get_3717490() {
		return _3717490;
	}
	//
	public String get_3731562() {
		return _3731562;
	}
	//
	public String get_3874544() {
		return _3874544;
	}
	//
	public String get_3896198() {
		return _3896198;
	}
	//
	public String get_3922818() {
		return _3922818;
	}
	//
	public String get_4030600() {
		return _4030600;
	}
	//
	public String get_4116284() {
		return _4116284;
	}
	//
	public String get_4217392() {
		return _4217392;
	}
	//
	public String get_4287320() {
		return _4287320;
	}
	//
	public String get_4425200() {
		return _4425200;
	}
	//
	public String get_4465564() {
		return _4465564;
	}
	//
	public String get_4481414() {
		return _4481414;
	}
	public String get_4581576() {
		return _4581576;
	}
	public String get_4630562() {
		return _4630562;
	}
	public String get_4849438() {
		return _4849438;
	}
	public String get_5052448() {
		return _5052448;
	}
	public String get_5155780() {
		return _5155780;
	}
	public String get_5442430() {
		return _5442430;
	}
	public String get_5460276() {
		return _5460276;
	}
	public String get_5607714() {
		return _5607714;
	}
	public String get_5710514() {
		return _5710514;
	}
	public String get_5982852() {
		return _5982852;
	}
	public Integer getMovieId() {
		return movieId;
	}

}

	
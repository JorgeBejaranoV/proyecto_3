package auxiliaresCarga;

public class DataFuncion 
{
	private String nombreTeatro;
	
	private DataFuncionesPelicula[] funciones;

	public String getNombreTeatro() {
		return nombreTeatro;
	}

	public void setNombreTeatro(String nombreTeatro) {
		this.nombreTeatro = nombreTeatro;
	}

	public DataFuncionesPelicula[] getFunciones() {
		return funciones;
	}

	public void setFunciones(DataFuncionesPelicula[] funciones) {
		this.funciones = funciones;
	}
}

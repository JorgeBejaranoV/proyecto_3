package auxiliaresCarga;

import com.google.gson.annotations.SerializedName;

public class DataRatingRequest 
{
	@SerializedName("item_id")
	private long movieId;
	
	@SerializedName("rating")
	private double rating;

	public long getMovieId() {
		return movieId;
	}

	public void setMovieId(long movieId) {
		this.movieId = movieId;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}
	
	
}

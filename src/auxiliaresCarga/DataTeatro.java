package auxiliaresCarga;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataTeatro {

	@SerializedName("Nombre")
	@Expose
	private String nombre;
	@SerializedName("UbicacionGeografica(Lat|Long)")
	@Expose
	private String ubicacionGeograficaLatLong;
	@SerializedName("Franquicia")
	@Expose
	private String franquicia;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUbicacionGeograficaLatLong() {
		return ubicacionGeograficaLatLong;
	}

	public void setUbicacionGeograficaLatLong(String ubicacionGeograficaLatLong) {
		this.ubicacionGeograficaLatLong = ubicacionGeograficaLatLong;
	}

	public String getFranquicia() {
		return franquicia;
	}

	public void setFranquicia(String franquicia) {
		this.franquicia = franquicia;
	}

}
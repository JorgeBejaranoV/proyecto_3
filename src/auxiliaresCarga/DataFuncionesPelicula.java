package auxiliaresCarga;

public class DataFuncionesPelicula
{
	private long movieId;
	
	private String[] horas;


	public long getMovieId() {
		return movieId;
	}

	public void setMovieId(long movieId) {
		this.movieId = movieId;
	}

	public String[] getHoras() {
		return horas;
	}

	public void setHoras(String[] horas) {
		this.horas = horas;
	}
	
	
}

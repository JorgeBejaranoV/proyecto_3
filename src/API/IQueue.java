package API;

public interface IQueue<T> {

	/**
	 * Agrega un elemento en la �ltima posici�n de la cola
	 * @param elemento a agregar
	 */
	public void enqueue(T item);
	
	/**
	 * Elimina el elemento en la primera posici�n de la cola
	 * @return Elemento eliminado 
	 */
	public T dequeue();
	
	/**
	 * Indica si la cola est� vac�a
	 * @return la cola est� vac�a?
	 */
	public boolean isEmpty();
	
	/**
	 * N�mero de elementos en la cola
	 * @return n�mero de elementos
	 */
	public int size();
	
}

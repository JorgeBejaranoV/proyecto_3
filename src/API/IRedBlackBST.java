package API;

import java.util.Iterator;


public interface IRedBlackBST <K, V> {
	
	public int size();
	
	public boolean isEmpty();
	
	public V get(K key) throws NoSuchElementException;
	
	public boolean contains(K key);
	
	public void put (K key, V val);
	
	public void deleteMin();
	
	public void deleteMax();
	
	public void delete(K key) throws NoSuchElementException;
	
	public int height();
	
	public K min();
	
	public K max();
	
	public boolean check();
	
	public Iterator<K> keys();
}

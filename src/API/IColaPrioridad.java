package API;

public interface IColaPrioridad<T> 
{
	/**
	 * Crea una cola de prioridad vac�a con un n�mero m�ximo de elementos
	 * @param max n�mero m�ximo de elementos
	 */
	public void crearCP(int max);
	
	/**
	 * Retorna n�mero de elementos en la cola de prioridad
	 * @return n�mero de elementos en la cola.
	 */
	
	public int darNumElementos();
	
	/**
	 * Agrega un elemento a la cola. Se genera Exception en el caso que se sobrepase el tama�o m�ximo de la cola.
	 * @throws Si sobre pasa el tama�o m�ximo de la cola.
	 * @param elemento elemento a agregar.
	 */
	
	public void agregar(T elemento) throws Exception;
	
	/**
	 * Saca/atiende el elemento m�ximo en la cola y lo retorna; null en caso de cola vac�a.	
	 * @return el m�ximo elemento de la cola.
	 */
	
	public T max();
	
	/**
	 * Retorna si la cola es vac�a o no.
	 * @return true si la cola est� vac�a. false si tiene al menos un elemento
	 */
	
	public boolean esVacia();
	
	/**
	 * Retorna el tama�o m�ximo de la cola.
	 * @return tama�o m�ximo de la cola.
	 */
	
	public int tamanoMax();
	
}

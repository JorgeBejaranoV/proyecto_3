package VOS;

public class VODistancia 
{
	private String teatro1;
	private String teatro2;
	private double distancia;
	
	public String getTeatro1() {
		return teatro1;
	}
	public void setTeatro1(String teatro1) {
		this.teatro1 = teatro1;
	}
	public String getTeatro2() {
		return teatro2;
	}
	public void setTeatro2(String teatro2) {
		this.teatro2 = teatro2;
	}
	public double getDistancia() {
		return distancia;
	}
	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}
	
	
}

package VOS;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOPeliculaPlan implements Comparable<VOPeliculaPlan> {
	
	/**
	 * Atributo que apunta a la pelicula que se propone
	 */
	private VOPelicula pelicula;
	
	
	/**
	 * Atributo que apunta al teatro que se propone
	 */
	private VOTeatro teatro;
	
	/**
	 * Atributo que modela la hora de inicio de la pelicula
	 */
	private Calendar horaInicio;
	
	/**
	 * Atributo que modela la hora de fin de la pelicula
	 */
	private Calendar horaFin;
	
	/**
	 * Atributo que modela el dia de presentacion de la pelicla
	 * (1..5)
	 */
	
	private int dia;
	
	public VOPeliculaPlan() {
		// TODO Auto-generated constructor stub
	}

	public VOPelicula getPelicula() {
		return pelicula;
	}

	public void setPelicula(VOPelicula pelicula) {
		this.pelicula = pelicula;
	}

	public VOTeatro getTeatro() {
		return teatro;
	}

	public void setTeatro(VOTeatro teatro) {
		this.teatro = teatro;
	}



	public Calendar getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(Calendar horaInicio) {
		this.horaInicio = horaInicio;
	}

	public Calendar getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(Calendar horaFin) {
		this.horaFin = horaFin;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}

	@Override
	public int compareTo(VOPeliculaPlan o) 
	{
		return getHoraInicio().compareTo(o.getHoraInicio());
	}
	
	
	

}

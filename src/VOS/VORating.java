package VOS;

public class VORating implements Comparable<VORating>
{
	private int userId;
	private int movieId;
	private double rating;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getMovieId() {
		return movieId;
	}
	public void setMovieId(int movieId) {
		this.movieId = movieId;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	@Override
	public int compareTo(VORating r) 
	{
		if(getRating() >= r.getRating())
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}
	
	
}

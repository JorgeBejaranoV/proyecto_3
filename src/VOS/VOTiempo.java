package VOS;

public class VOTiempo 
{
	private int tiempo;
	private String origen;
	private String destino;
	
	public int getTiempo() 
	{
		return tiempo;
	}
	public void setTiempo(int tiempo) 
	{
		this.tiempo = tiempo;
	}
	public String getOrigen() 
	{
		return origen;
	}
	public void setOrigen(String origen) 
	{
		this.origen = origen;
	}
	public String getDestino() 
	{
		return destino;
	}
	public void setDestino(String destino) 
	{
		this.destino = destino;
	}
	
	
}

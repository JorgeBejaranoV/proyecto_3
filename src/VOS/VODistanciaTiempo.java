package VOS;

/**
 * Clase que modela la uni�n entre los teatros
 */
public class VODistanciaTiempo implements Comparable<VODistanciaTiempo>
{
	/**
	 * Nombre del teatro de origen
	 */
	private String origen;
	
	/**
	 * Nombre del teatro de destino
	 */
	private String destino;
	
	/**
	 * Tiempo de desplazamiento en minutos
	 */
	private int tiempo;
	
	/**
	 * Distancia de recorrido en kil�metros
	 */
	private double distancia;
	
	/**
	 * Constructor
	 */
	public VODistanciaTiempo(){	}

	public String getOrigen() 
	{
		return origen;
	}

	public void setOrigen(String origen)
	{
		this.origen = origen;
	}

	public String getDestino() 
	{
		return destino;
	}

	public void setDestino(String destino) 
	{
		this.destino = destino;
	}

	public int getTiempo() 
	{
		return tiempo;
	}

	public void setTiempo(int tiempo) 
	{
		this.tiempo = tiempo;
	}

	public double getDistancia() 
	{
		return distancia;
	}

	public void setDistancia(double distancia) 
	{
		this.distancia = distancia;
	}

	/**
	 * M�todo para comparar los valores de distancia y tiempo
	 */
	@Override
	public int compareTo(VODistanciaTiempo dist2) 
	{
		double distancia1 = getDistancia();
		int tiempo1 = getTiempo();
		double distancia2 = dist2.getDistancia();
		int tiempo2 = dist2.getTiempo();
		
		if(tiempo1 > tiempo2)
		{
			return 1;
		}
		else if(tiempo2 > tiempo1)
		{
			return -1;
		}
		else
		{
			if(distancia1 > distancia2)
			{
				return 1;
			}
			else
			{
				return -1;
			}
		}
	}
	
	
	
}

package VOS;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOUsuario 
{
	
	/**
	 * Atributo que modela el id del usuario
	 */

	private int id;
	
	public VOUsuario() 
	{
		// TODO Auto-generated constructor stub
	}
	
	public int getId()
	{
		return id;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
}

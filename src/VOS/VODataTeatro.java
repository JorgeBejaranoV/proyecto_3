package VOS;

import com.google.gson.annotations.SerializedName;

public class VODataTeatro 
{
	@SerializedName("")
	private String nombre;
	
	@SerializedName("UbicacionGeografica(Lat|Long)")
	private String ubicacion;
	
	@SerializedName("Franquicia")
	private String franquicia;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getFranquicia() {
		return franquicia;
	}

	public void setFranquicia(String franquicia) {
		this.franquicia = franquicia;
	}
}

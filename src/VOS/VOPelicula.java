package VOS;

public class VOPelicula {
	
	private long id;
	private String titulo;
	private String[] generos;
	
	
	
	public VOPelicula(int id, String titulo, String[] generos) {
	
		this.id = id;
		this.titulo = titulo;
		this.generos = generos;
	}
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String[] getGeneros() {
		return generos;
	}
	public void setGeneros(String[] generos) {
		this.generos = generos;
	}
	
	

}
